const webpack = require('webpack');
const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const InjectAssetsWebpackPlugin = require('inject-assets-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const extractCss = new ExtractTextPlugin({
  filename: "styles/[name].[hash].css",
});

const webpackConfigBase = config => ({
  entry: {
    app: ['./src/bootstrap.js'],
    vendor: [
      'react',
      'react-dom',
      'react-router-dom',
      'prop-types',
    ],
  },

  output: {
    path: config.public,
    filename: 'js/[name].[chunkhash].js',
    chunkFilename: 'js/[name].[chunkhash].js',
    publicPath: '/',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: config.babelExlude ? config.babelExlude : /xxx/,
        loader: 'babel-loader',
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.(less|css)$/,
        use: extractCss.extract({
          use: [
            {
              loader: "css-loader",
            },
            {
              loader: "less-loader",
            },
          ],
          // use style-loader in development
          fallback: "style-loader",
        }),
      },
      {
        test: /\.(png|svg|gif)$/,
        exclude: [
          /fonts/,
        ],
        loaders: [
          'file-loader?name=images/[name].[ext]',
        ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)/,
        exclude: [
          /images/,
          /flags/,
        ],
        loaders: [
          'file-loader?name=fonts/[name].[ext]',
        ],
      },
      {
        test: /\.(mp3|wav)/,
        loaders: [
          'file-loader?name=files/[name].[ext]',
        ],
      },
    ],
  },

  externals: {
    AppConfig: JSON.stringify(config.app),
  },

  devtool: 'source-map',

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "all",
        },
      },
    },
  },
});

const webpackPluginsBase = (config, options) => {
  let plugins = [
    new webpack.ProvidePlugin({
      'React': 'react',
    }),
  ];

  const opts = Object.assign({
    assets: true,
    css: true,
    sw: true,
  }, options);

  if (opts.assets) {
    plugins.push(
      new HtmlWebpackPlugin({
        title: config.app.appName,
        filename: 'index.html',
        template: path.join(__dirname, '/assets/html/index.ejs'),
        inject: true,
        chunks: ['app', 'vendor'],
      })
    );
  }

  if (opts.css) {
    plugins.push(
      extractCss
    );
  }

  if (opts.sw) {
    plugins.push(
      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, '/assets/html/.htaccess'),
          to: config.public,
        },
        {
          from: path.join(__dirname, '/assets/serviceWorkers'),
          to: config.public,
        },
      ], {
        ignore: [
          '*.ejs',
        ],
      })
    );

    plugins.push(
      new InjectAssetsWebpackPlugin({
        filename: path.join(config.public, 'worker.js'),
      },[
        {
          pattern: '{hash}',
          type: 'hash',
        },
        {
          pattern: '{project}',
          type: 'value',
          value: config.app.appName,
        },
        {
          pattern: '{hostname}',
          type: 'value',
          value: config.app.hostname,
        },
        {
          pattern: '{files_to_cache}',
          type: 'chunks',
          chunks: ['app', 'vendor'],
          files: ['.js', '.css'],
          excludeFiles: ['.map'],
          decorator: fileNames => fileNames.join('\', \''),
        },
      ])
    );
  }

  return plugins;
};

const webpackConfigDev = config => Object.assign({}, webpackConfigBase(config), {
  cache: true,
  mode: "development",

  devServer: {
    disableHostCheck: true,
    contentBase: config.public,
    port: typeof config.app.webpackDevServerPort !== 'undefined' ? config.app.webpackDevServerPort : 8080,
    public: typeof config.app.webpackDevServerPublic !== 'undefined' ? config.app.webpackDevServerPublic : 'ui.dev',
    host: typeof config.app.webpackDevServerHost !== 'undefined' ? config.app.webpackDevServerHost : '0.0.0.0',
    inline: true,
    historyApiFallback: true,
    noInfo: false,
  },
});

const pluginsDev = (config, options) => ([
  ...webpackPluginsBase(config, options),
]);

const webpackConfigPro = config => Object.assign({}, webpackConfigBase(config), {
  cache: false,
  mode: "production",
});

const pluginsPro = (config, options) => ([
  ...webpackPluginsBase(config, options),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production'),
    },
  }),
  new webpack.optimize.AggressiveMergingPlugin(),
]);

const pluginsAnalyze = (config, options) => ([
  ...webpackPluginsBase(config, options),
  new BundleAnalyzerPlugin({
    analyzerMode: 'server',
    analyzerHost: '127.0.0.1',
    analyzerPort: 8888,
    reportFilename: 'report.html',
    defaultSizes: 'parsed',
    openAnalyzer: true,
    generateStatsFile: false,
    statsFilename: 'stats.json',
    statsOptions: null,
    logLevel: 'info',
  }),
]);

module.exports = {
  webpackConfigDev,
  pluginsDev,
  webpackConfigPro,
  pluginsPro,
  pluginsAnalyze,
};
