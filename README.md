Include SDK as a dependency in your package.json

```json
{
  ...
  "dependencies": {
    ...
    "cursol": "git+https://stash.cursol.io/scm/mod/cursol-ui-sdk.git"
  },
  ...
}
```

Import files from sdk folder

```javascript
import RestList from 'cursol/sdk/rest/containers/RestList';
```

Delete yarn.lock file in project on SDK update

### Migration to material-ui 0.35

<Button raised   ->   <Button variant='raised'
<Button fab   ->   <Button variant='fab'
<Typography type=  ->   <Typography variant=
color='accent'   ->    color='secondary'
color='contrast'   ->    color='inherit'
onRequestClose=   ->    onClose=
<ListItem   ->    <ListItem button

### Migrate to @material/core v1.x
update material to @material-ui/core
update icons to @material-ui/icons

npm install -g jscodeshift
npm install @material-ui/codemod

replace all material-ui/ to @material-ui/core/
find src -name '*.js' -print | xargs jscodeshift -t node_modules/@material-ui/codemod/lib/v1.0.0/import-path.js
find src -name '*.js' -print | xargs jscodeshift -t node_modules/@material-ui/codemod/lib/v1.0.0/color-imports.js

replace all material-ui-icons/ to @material-ui/icons/
find src -name '*.js' -print | xargs jscodeshift -t node_modules/@material-ui/codemod/lib/v1.0.0/svg-icon-imports.js

<Dialog transition=     ->       <Dialog TransitionComponent=

### Migrate to @material/core v4+
theme.spacing.unit\s*\*\s*(\d+)     ->      theme.spacing($1)
theme.spacing.unit\s*\/\s*(\d+)     ->      theme.spacing(1/$1)
theme.spacing.unit                  ->      theme.spacing(1)
spacing={8}                         ->      spacing={1}
spacing={16}                        ->      spacing={2}
variant='display1'                  ->      variant='h4'
variant='title'                     ->      variant='h5'
variant='raised'                    ->      variant='contained'
variant='flat'                      ->      variant='text'
<Button variant='fab'               ->      <Fab      
<Grid container>                    ->      <Grid container spacing={1}>
