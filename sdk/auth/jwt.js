import jwtDecode from 'jwt-decode'
import { wsLogin, wsLogout } from './../ws/ws'

let storage = localStorage

export const TOKEN_SESSION_KEY = 'authJwt'

export const useSessionStorage = () => {
  storage = sessionStorage
}

export const hasToken = () => {
  const token = storage.getItem(TOKEN_SESSION_KEY)
  return token && token.length > 16
}

export const isTokenExpired = () => {
  if (!hasToken()) {
    return true
  }

  const payload = jwtDecode(getToken())
  const time = new Date().getTime()
  const jwtExpireTime = payload.exp * 1000
  return jwtExpireTime < time
}

export const getToken = () => {
  return storage.getItem(TOKEN_SESSION_KEY)
}

export const setToken = token => {
  if (!token) {
    storage.removeItem(TOKEN_SESSION_KEY)
    wsLogout()
    return
  }

  storage.setItem(TOKEN_SESSION_KEY, token)
  wsLogin(token)
}
