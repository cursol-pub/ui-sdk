import { getRequest, postRequest, putRequest } from './../../rest/requests'
import { setToken } from './../jwt'
import { getDeviceFingerprintPromise } from './../../device/fingerprint'

export const SIGNIN_POST = 'SIGNIN_POST'
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS'
export const SIGNIN_FAIL = 'SIGNIN_FAIL'
export const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS'
export const AUTH_USERBYTOKEN_GET = 'AUTH_USERBYTOKEN_GET'
export const AUTH_USERBYTOKEN_SUCCESS = 'AUTH_USERBYTOKEN_SUCCESS'
export const AUTH_USERBYTOKEN_FAIL = 'AUTH_USERBYTOKEN_FAIL'
export const AUTH_GET_TOKEN = 'AUTH_GET_TOKEN'
export const AUTH_GET_TOKEN_SUCCESS = 'AUTH_GET_TOKEN_SUCCESS'
export const AUTH_GET_TOKEN_FAIL = 'AUTH_GET_TOKEN_FAIL'
export const AUTH_UPDATE_TOKEN = 'AUTH_UPDATE_TOKEN'
export const AUTH_UPDATE_TOKEN_SUCCESS = 'AUTH_UPDATE_TOKEN_SUCCESS'
export const AUTH_UPDATE_TOKEN_FAIL = 'AUTH_UPDATE_TOKEN_FAIL'

export const postSignIn = formData => dispatch => {
  dispatch({
    type: SIGNIN_POST,
    data: formData,
  })

  return postRequest('signin', formData)
    .then(data => {
      if (!data.token) {
        setToken(null)
        return dispatch({
          type: SIGNIN_FAIL,
          error: 'No authorisation token',
        })
      }

      setToken(data.token)
      dispatch({
        type: SIGNIN_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: SIGNIN_FAIL,
        error,
      })
    })
}

export const getToken = (url = null, payload) => dispatch => {
  dispatch({
    type: AUTH_GET_TOKEN,
  })

  return getDeviceFingerprintPromise()
    .then(deviceFingerprint => {
      return postRequest(
        url || 'auth/token',
        Object.assign(payload || {}, { device_fingerprint: deviceFingerprint })
      ).then(data => {
        if (!data.token) {
          setToken(null)
          return dispatch({
            type: AUTH_GET_TOKEN_FAIL,
            error: 'No authorisation token',
          })
        }

        setToken(data.token)
        dispatch({
          type: AUTH_GET_TOKEN_SUCCESS,
          data,
        })
      })
    })
    .catch(error => {
      dispatch({
        type: AUTH_GET_TOKEN_FAIL,
        error,
      })
    })
}

export const refreshToken = (url = null) => dispatch => {
  dispatch({
    type: AUTH_GET_TOKEN,
  })

  return getRequest(url || 'auth/refresh')
    .then(data => {
      if (!data.token) {
        setToken(null)
        return dispatch({
          type: AUTH_GET_TOKEN_FAIL,
          error: 'No authorisation token',
        })
      }

      setToken(data.token)
      dispatch({
        type: AUTH_GET_TOKEN_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: AUTH_GET_TOKEN_FAIL,
        error,
      })
    })
}

export const updateToken = (url = null, payload) => dispatch => {
  dispatch({
    type: AUTH_UPDATE_TOKEN,
  })

  return putRequest(url || 'auth/token', payload)
    .then(data => {
      if (!data.token) {
        setToken(null)
        return dispatch({
          type: AUTH_UPDATE_TOKEN_FAIL,
          error: 'No authorisation token',
        })
      }

      setToken(data.token)
      dispatch({
        type: AUTH_UPDATE_TOKEN_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: AUTH_UPDATE_TOKEN_FAIL,
        error,
      })
    })
}

export const signInByToken = token => dispatch => {
  setToken(token)

  dispatch(getUser())
}

export const signOut = () => dispatch => {
  setToken(null)

  dispatch({
    type: SIGNOUT_SUCCESS,
  })
}

export const getUser = () => dispatch => {
  dispatch({
    type: AUTH_USERBYTOKEN_GET,
  })

  return getRequest('get-user')
    .then(data => {
      if (!data.user) {
        // If we cannot get user with current token - reset token to sign in again
        setToken(null)

        return dispatch({
          type: AUTH_USERBYTOKEN_FAIL,
          error: 'Wrong authorisation token',
        })
      }

      dispatch({
        type: AUTH_USERBYTOKEN_SUCCESS,
        data,
      })
    })
    .catch(error => {
      // If we cannot get user with current token - reset token to sign in again
      setToken(null)

      dispatch({
        type: AUTH_USERBYTOKEN_FAIL,
        error,
      })
    })
}
