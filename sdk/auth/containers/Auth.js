import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router'

import { hasToken } from './../jwt'
import { getUser, signOut } from './../actions/auth'
import shouldComponentUpdate from './../../form/redux-form/shouldComponentUpdate'

class AuthContainer extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    onGetUser: PropTypes.func.isRequired,
    onSignOut: PropTypes.func.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    isAuthorised: PropTypes.bool.isRequired,
    userData: PropTypes.object,
    user: PropTypes.bool,
    guest: PropTypes.bool,
    redirect: PropTypes.oneOfType([PropTypes.bool, PropTypes.object, PropTypes.string]),
    location: PropTypes.object,
  }

  static defaultProps = {
    user: false,
    guest: false,
  }

  componentDidMount() {
    this.getUserData(this.props)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isAuthorised === false && this.props.isAuthorised === true) {
      this.getUserData(this.props)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      shouldComponentUpdate(this.props, nextProps, ['isSubmitting', 'isAuthorised', 'userData']) ||
      this.props.children !== nextProps.children
    )
  }

  getUserData(props) {
    const { isSubmitting, onGetUser, onSignOut } = props

    if (isSubmitting) {
      return
    }

    if (this.isAuthorizedUserTokenMissed(props)) {
      onSignOut()
    }

    if (this.isAwaitingUserData(props)) {
      onGetUser()
    }
  }

  isAwaitingUserData(props) {
    const { isAuthorised, userData } = props

    return hasToken() && !(isAuthorised && userData)
  }

  isAuthorizedUserTokenMissed(props) {
    const { isAuthorised } = props

    return isAuthorised && !hasToken()
  }

  isAccessRestricted() {
    const { isAuthorised, user: userRequired, guest: guestRequired } = this.props

    return (userRequired && !isAuthorised) || (guestRequired && isAuthorised)
  }

  render() {
    const { children } = this.props

    if (this.isAccessRestricted()) {
      return this.renderFail()
    }

    return children.length ? <span>{children}</span> : children
  }

  renderFail() {
    const { user: userRequired, redirect, location } = this.props

    if (!redirect || this.isAwaitingUserData(this.props)) {
      return null
    }

    if (userRequired) {
      const loginRedirect = {
        pathname: '/login',
        state: { from: location },
      }

      return <Redirect to={redirect !== true ? redirect : loginRedirect} />
    }

    return <Redirect to={redirect !== true ? redirect : '/'} />
  }
}

const mapStateToProps = state => {
  const {
    auth: { isSubmitting, isAuthorised, user: userData },
  } = state
  return {
    isSubmitting,
    isAuthorised,
    userData,
  }
}

const mapDispatchToProps = dispatch => ({
  onGetUser: () => dispatch(getUser()),
  onSignOut: () => dispatch(signOut()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AuthContainer))
