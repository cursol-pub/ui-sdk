export const parseQueryString = (query, decodeFirst = true) => {
  const q = decodeFirst ? decodeURIComponent(query) : query
  return q
    .replace(/^\?/, '')
    .split('&')
    .map(item => {
      return /([^=]+)=(.*)/.exec(item)
    })
    .reduce((result, itemParts) => {
      if (!Array.isArray(itemParts)) {
        return result
      }
      const [input, name, value] = itemParts
      if (input && name) {
        return Object.assign({}, result, {
          [name]: value ? (decodeFirst ? value : decodeURIComponent(value)) : true,
        })
      }
      return result
    }, {})
}

export const flip = obj =>
  Object.keys(obj).reduce((out, key) => {
    return {
      ...out,
      [obj[key]]: key,
    }
  }, {})

export const sortByProperty = propName => (a, b) => {
  if (!a || !b || !a[propName] || !b[propName]) {
    return 0
  }

  if (a.country && `${a.country}`.toLowerCase() === 'gb') {
    return -1
  }

  if (b.country && `${b.country}`.toLowerCase() === 'gb') {
    return 1
  }

  if (`${a[propName]}`.toLowerCase() > `${b[propName]}`.toLowerCase()) {
    return 1
  }

  if (`${a[propName]}`.toLowerCase() < `${b[propName]}`.toLowerCase()) {
    return -1
  }

  return 0
}
