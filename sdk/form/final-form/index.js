import { Field, FormSpy } from 'react-final-form'
import { OnChange } from 'react-final-form-listeners'

const subsciptions = {}

export const addListener = (eventName, callback) => {
  if (typeof callback !== 'function') {
    throw new Error('Listener must be a function')
  }
  if (!Array.isArray(subsciptions[eventName])) {
    subsciptions[eventName] = []
  }
  subsciptions[eventName].push(callback)
}

export const publish = (eventName, data) => {
  if (subsciptions[eventName] && subsciptions[eventName].length) {
    const callbacks = subsciptions[eventName]
    for (let i = 0; i < callbacks.length; i++) {
      callbacks[i](data)
    }
  }
}

export const ListenEvent = ({ eventName, children }) => {
  addListener(eventName, children)
  return <div />
}

export const SetFieldOnEvent = ({ eventName, field, clearTouched }) => (
  <Field name={field} subscription={{}}>
    {(
      // No subscription. We only use Field to get to the change function
      { input: { onChange } }
    ) => (
      <FormSpy subscription={{}}>
        {({ form }) => (
          <ListenEvent eventName={eventName}>
            {data => {
              if (data && data[field]) {
                if (clearTouched) {
                  form.resetFieldState(field)
                }
                onChange(data[field])
              }
            }}
          </ListenEvent>
        )}
      </FormSpy>
    )}
  </Field>
)

export const SetFieldOnOtherChanges = ({ field, becomes, set, to, clearTouched }) => (
  <Field name={set} subscription={{}}>
    {(
      // No subscription. We only use Field to get to the change function
      { input: { onChange } }
    ) => (
      <FormSpy subscription={{}}>
        {({ form }) => (
          <OnChange name={field}>
            {value => {
              if (value === becomes) {
                if (clearTouched) {
                  form.resetFieldState(set)
                }
                onChange(to)
              }
            }}
          </OnChange>
        )}
      </FormSpy>
    )}
  </Field>
)
