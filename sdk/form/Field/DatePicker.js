import DatePickerField from './../components/DatePickerField'
import FieldWrapper from './../redux-form/Wrapper'

const DatePicker = props => {
  return <FieldWrapper component={DatePickerField} {...props} />
}

export default DatePicker
