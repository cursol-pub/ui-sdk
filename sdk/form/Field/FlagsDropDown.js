import FlagsDropDownField from './../components/FlagsDropDownField'
import FieldWrapper from './../redux-form/Wrapper'

const FlagsDropDown = props => {
  return <FieldWrapper component={FlagsDropDownField} {...props} />
}

export default FlagsDropDown
