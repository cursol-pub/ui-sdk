import MuiTextField from '@material-ui/core/TextField'
import FieldWrapper from './../redux-form/Wrapper'

const TextField = props => {
  return <FieldWrapper component={MuiTextField} autoComplete={'none'} {...props} />
}

export default TextField
