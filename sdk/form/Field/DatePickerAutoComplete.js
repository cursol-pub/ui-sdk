import DatePickerAutoCompleteField from './../components/DatePickerAutoCompleteField'
import FieldWrapper from './../redux-form/Wrapper'

const DatePicker = props => {
  return <FieldWrapper component={DatePickerAutoCompleteField} {...props} />
}

export default DatePicker
