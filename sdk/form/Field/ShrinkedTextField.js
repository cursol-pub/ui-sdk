import MuiTextField from '@material-ui/core/TextField'
import FieldWrapper from './../redux-form/Wrapper'

const ShrinkedTextField = props => {
  return (
    <FieldWrapper
      component={MuiTextField}
      autoComplete={'none'}
      InputLabelProps={{
        shrink: true,
      }}
      {...props}
    />
  )
}

export default ShrinkedTextField
