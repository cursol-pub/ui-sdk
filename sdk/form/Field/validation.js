export const ERROR_VALIDATION_CODE = 'ERROR_VALIDATION'

export const isRequired = fieldName => (value, allValues, props) => {
  if (Array.isArray(value) && !value.length) {
    return `Select at least one ${fieldName}`
  }

  if (!value || value === '') {
    return `Enter ${fieldName}`
  }

  return undefined
}

export const isRequiredLetterCase = fieldName => (value, allValues, props) => {
  if (!value || value === '') {
    return `Enter ${fieldName}`
  }

  if (value === value.toUpperCase() && value.charAt(0) !== value.toUpperCase()) {
    return `You have used capitals in your name`
  } else if (value.charAt(0) !== value.charAt(0).toUpperCase()) {
    return `First letter of your name is not capital`
  }

  return undefined
}

export const parseValidationErrors = (error, formValues = {}) => {
  if (error && error.code === ERROR_VALIDATION_CODE && error.message && typeof error.message === 'object') {
    return Object.keys(error.message).reduce((errors, fieldName) => {
      if (!errors[fieldName]) {
        let message = error.message[fieldName]
        if (typeof message === 'object') {
          message = Object.keys(message).reduce((result, key) => {
            return `${result}${message[key]}`
          }, '')
        }
        errors[fieldName] = {
          message,
          value: formValues[fieldName],
        }
      }
      return errors
    }, {})
  }
  return {}
}
