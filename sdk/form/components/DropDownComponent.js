import { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Scrollbars } from 'react-custom-scrollbars'

import Input from '@material-ui/core/Input'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'
import IconButton from '@material-ui/core/IconButton'
import KeyboardArrowDownIcon from '@material-ui/icons/ArrowDropDown'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  chip: {
    margin: theme.spacing(1 / 2),
  },
  chipItem: {
    paddingRight: 42,
  },
  chips: {
    marginTop: theme.spacing(1 / 2),
    marginBottom: theme.spacing(-1 / 2),
  },
  icon: {
    position: 'absolute',
    right: -4,
    top: -4,
    transition: theme.transitions.create(['transform'], {
      duration: theme.transitions.duration.shorter,
    }),
  },
  iconOpened: {
    transform: 'rotate(180deg)',
  },
  iconClosed: {
    transform: 'rotate(0deg)',
  },
  dropdown: {
    position: 'absolute',
    zIndex: 1600,
    top: 40,
    left: 0,
    width: '100%',
    overflowY: 'auto',
  },
  closed: {
    left: -5000,
  },
  scrollbars: {
    height: 300,
    width: '100%',
    position: 'relative',
    overflow: 'hidden',
  },
  inputWrapper: {
    position: 'relative',
  },
  input: {
    width: '100%',
  },
})

const filterOptionsDefault = (options, value, filter = '') => {
  const selected = value
    ? Array.isArray(value)
      ? value.map(item => `${item.value}`.toLowerCase())
      : [`${value.value}`.toLowerCase()]
    : []
  return options.filter(item => {
    if (selected.indexOf(`${item.value}`.toLowerCase()) > -1) {
      return false
    }

    if (filter === '') {
      return true
    }

    if (
      `${item.value}`.toLowerCase().indexOf(filter.toLowerCase()) > -1 ||
      `${item.label}`.toLowerCase().indexOf(filter.toLowerCase()) > -1
    ) {
      return true
    }

    return false
  })
}
const stringifyItemDefault = item => (item ? item.label : '')
const getItemValueDefault = item => (item ? item.value : '')
const getItemByValueDefault = options => value => {
  return Array.isArray(options)
    ? options.reduce((item, option) => {
        if (option.value === value) {
          return option
        }
        return item
      }, {})
    : {}
}

class DropDownComponent extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    options: PropTypes.array.isRequired,
    label: PropTypes.string,
    helperText: PropTypes.object,
    multi: PropTypes.bool.isRequired,
    limit: PropTypes.number,
    autocomplete: PropTypes.bool.isRequired,
    InputProps: PropTypes.object,
    DropDownProps: PropTypes.object,
    ItemProps: PropTypes.object,
    inputWrapperClass: PropTypes.string,
    dropDownClass: PropTypes.string,
    dropDownItemClass: PropTypes.string,
    iconClass: PropTypes.string,
    chipsClass: PropTypes.string,
    chipClass: PropTypes.string,
    InputComponent: PropTypes.any,
    ItemComponent: PropTypes.any,
    IconComponent: PropTypes.any,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    onSelect: PropTypes.func,
    filterOptions: PropTypes.func,
    stringifyItem: PropTypes.func,
    getItemValue: PropTypes.func,
    getItemByValue: PropTypes.func,
  }

  static defaultProps = {
    multi: false,
    autocomplete: true,
  }

  state = {
    open: false,
    filter: '',
    value: null,
    labelWidth: 0,
  }

  inputRef = null
  isInputFocused = false
  isOnDropdown = false
  id = Math.floor(Math.random() * 1e10)
  choosenValue = null
  isMobile = false

  componentDidMount() {
    const { InputProps, options } = this.props
    if (!InputProps) {
      return
    }
    const { value: inpValue } = InputProps

    if (inpValue) {
      if (options.length) {
        this.setValue(this.getItemByValue(inpValue))
      } else {
        this.choosenValue = inpValue
      }
    }

    const viewWidth = window.innerWidth || document.documentElement.clientWidth
    this.isMobile = viewWidth <= 800
    if (this.isMobile) {
      document.addEventListener('click', this.onDocumentClick)
    }
  }

  componentDidUpdate(prevProps) {
    const { InputProps, options } = this.props
    const { InputProps: prevInputProps, options: prevOptions } = prevProps

    if (
      InputProps &&
      (!prevInputProps || prevInputProps.value !== InputProps.value) &&
      InputProps.value !== this.choosenValue
    ) {
      if (options.length) {
        this.setValue(this.getItemByValue(InputProps.value))
      } else {
        this.choosenValue = InputProps.value
      }
    }

    if (!prevOptions.length && options.length && this.choosenValue) {
      this.setValue(this.getItemByValue(this.choosenValue))
    }

    this.adjustDropdownHeight()
  }

  componentWillUnmount() {
    if (this.isMobile) {
      document.removeEventListener('click', this.onDocumentClick)
    }
  }

  onDocumentClick = e => {
    if (this.state.open && !this.isOnDropdown) {
      this.closeDropDown(true)
    }
  }

  adjustDropdownHeight = () => {
    const cont = document.getElementById(`dropdown-content-${this.id}`)
    const scrollArea = document.getElementById(`dropdown-scroll-${this.id}`)

    if (!cont || !scrollArea) {
      return
    }

    const contentHeight = cont.offsetHeight > 300 ? 300 : cont.offsetHeight
    scrollArea.setAttribute('style', `height: ${contentHeight}px !important`)
  }

  handleFocus = () => {
    this.isInputFocused = true
    this.isOnDropdown = true
    if (!this.state.open) {
      this.setState({ open: true, filter: '' })
      if (this.props.onOpen) {
        this.props.onOpen()
      }
    }
  }

  handleChange = event => {
    const { autocomplete } = this.props
    if (!autocomplete) {
      return
    }

    const filter = event.target.value
    this.setState({ filter })
  }

  handleChangeMobile = event => {
    this.setValue(this.getItemByValue(event.target.value))
  }

  handleClickItem = item => {
    const { multi, onSelect } = this.props

    if (multi) {
      this.addValue(item)
    } else {
      this.setValue(item)
    }

    if (onSelect) {
      onSelect(item)
    }

    this.closeDropDown(false)
  }

  handleRemoveItem = value => {
    this.removeValue(value)
  }

  handleBlur = () => {
    this.isInputFocused = false
    if (!this.isOnDropdown) {
      this.closeDropDown()
    }
  }

  handleIconClick = () => {
    if (this.state.open) {
      this.closeDropDown()
    } else {
      this.focusFilter()
    }
  }

  handleMouseOver = () => {
    this.isOnDropdown = true
  }

  handleMouseOut = () => {
    this.isOnDropdown = false
  }

  handleDropDownMouseOver = event => {
    this.isOnDropdown = true
    this.focusFilter()
  }

  handleDropDownMouseOut = event => {
    this.isOnDropdown = false
    setTimeout(() => {
      if (!this.isInputFocused) {
        this.closeDropDown()
      }
    }, 10)
  }

  focusFilter = () => {
    if (this.inputRef) {
      this.inputRef.focus()
    }
  }

  closeDropDown(getValueBack = true) {
    const { multi, onClose } = this.props
    const { value } = this.state

    this.isOnDropdown = false
    const newState = {
      open: false,
    }

    if (getValueBack) {
      newState.filter = multi ? '' : this.stringifyItem(value)
    }
    this.setState(newState)

    if (onClose) {
      onClose()
    }
  }

  addValue(value) {
    const { limit } = this.props
    const { value: currentValue } = this.state

    const newValue = Array.isArray(currentValue) ? currentValue.slice() : []

    if (limit && newValue.length >= limit) {
      return
    }

    newValue.push(value)
    this.setValue(newValue)
  }

  removeValue(value) {
    const { value: currentValue } = this.state
    const newValue = currentValue.filter(item => item !== value)
    this.setValue(newValue)
  }

  setValue(value) {
    this.choosenValue = this.getItemValue(value)
    const {
      InputProps: { onChange },
      multi,
    } = this.props

    if (onChange) {
      onChange(this.getItemValue(value))
    }

    this.setState({
      filter: multi ? '' : this.stringifyItem(value),
      value: value,
    })

    this.scrollElement()
  }

  scrollElement = () => {
    if (!this.isMobile) {
      return
    }

    const {
      InputProps: { name },
    } = this.props

    const el = document.querySelector(`[name="${name}"]`)
    if (!el || !el.parentNode) {
      return
    }

    const wscroll = window.pageYOffset || document.documentElement.scrollTop
    const targetPos = el.parentNode.getBoundingClientRect().top + wscroll - window.innerHeight + 180

    if (targetPos > wscroll) {
      this.scrollOneStep(targetPos, (targetPos - wscroll) / 6)()
    }
  }

  scrollOneStep = (targetPos, step) => () => {
    const wscroll = window.pageYOffset || document.documentElement.scrollTop
    if (targetPos <= wscroll) {
      return
    }

    window.scroll(0, wscroll + step)
    setTimeout(this.scrollOneStep(targetPos, step), 50)
  }

  getItemValue = (value, single = false) => {
    const { getItemValue, multi } = this.props
    const valF = getItemValue || getItemValueDefault
    return multi && !single ? (Array.isArray(value) ? value.map(valF) : []) : valF(value)
  }

  getItemByValue = value => {
    const { getItemByValue, multi, options } = this.props
    const itemF = getItemByValue ? getItemByValue(options) : getItemByValueDefault(options)
    return multi ? (Array.isArray(value) ? value.map(itemF) : []) : itemF(value)
  }

  stringifyItem = value => {
    const { stringifyItem, multi } = this.props
    const stringifyItemF = stringifyItem || stringifyItemDefault
    return multi
      ? Array.isArray(value)
        ? value.map(stringifyItemF).join(', ')
        : stringifyItemF(value)
      : stringifyItemF(value)
  }

  updateLabelWidth = node => {
    if (!node) {
      return
    }

    const { labelWidth } = this.state
    if (labelWidth !== node.offsetWidth) {
      this.setState({
        labelWidth: node.offsetWidth,
      })
    }
  }

  renderMobile = () => {
    const {
      classes,
      className,
      inputWrapperClass,
      dropDownClass,
      dropDownItemClass,
      iconClass,
      chipsClass,
      chipClass,
      label,
      helperText,
      options,
      limit,
      InputProps,
      DropDownProps,
      ItemProps,
      InputComponent,
      IconComponent,
      ItemComponent,
      multi,
      autocomplete,
      filterOptions,
      placeholder,
      onSelect,
      onOpen,
      onClose,
      ...other
    } = this.props
    const { name, value: inputValue, onFocus, onBlur, onChange, ...otherInputProps } = InputProps
    const { value, labelWidth } = this.state

    const DefaultItemComponent = ({ item }) => <span>{this.stringifyItem(item)}</span>
    const Item = ItemComponent || DefaultItemComponent
    const InputVariant = other && other.variant === 'outlined' ? OutlinedInput : Input

    const filterField = (
      <div className={classes.inputWrapper}>
        {label ? (
          <InputLabel shrink={!!value} htmlFor={`${name}-field`} ref={this.updateLabelWidth}>
            {label}
          </InputLabel>
        ) : null}

        <Select
          multiple={multi}
          input={
            <InputVariant
              id={`${name}-field`}
              name={name}
              type={'text'}
              className={classes.input}
              labelWidth={labelWidth}
              {...otherInputProps}
            />
          }
          value={this.getItemValue(value)}
          onChange={this.handleChangeMobile}
          renderValue={v => this.stringifyItem(this.getItemByValue(v))}
          placeholder={placeholder}
        >
          {options.map((item, key) => (
            <MenuItem className={dropDownItemClass} key={key} value={this.getItemValue(item, true)}>
              <Item item={item} {...ItemProps} />
            </MenuItem>
          ))}
        </Select>
      </div>
    )

    return (
      <FormControl className={className} {...other}>
        {InputComponent ? (
          <InputComponent item={value} {...ItemProps}>
            {filterField}
          </InputComponent>
        ) : (
          filterField
        )}

        {helperText ? <FormHelperText>{helperText}</FormHelperText> : null}
      </FormControl>
    )
  }

  render() {
    const {
      classes,
      className,
      inputWrapperClass,
      dropDownClass,
      dropDownItemClass,
      iconClass,
      chipsClass,
      chipClass,
      label,
      helperText,
      options,
      limit,
      InputProps,
      DropDownProps,
      ItemProps,
      InputComponent,
      IconComponent,
      ItemComponent,
      multi,
      autocomplete,
      filterOptions,
      placeholder,
      onSelect,
      onOpen,
      onClose,
      ...other
    } = this.props
    const { name, value: inputValue, onFocus, onBlur, onChange, ...otherInputProps } = InputProps
    const { open, filter, value, labelWidth } = this.state

    if (this.isMobile && !multi) {
      // return this.renderMobile()
    }

    const filterOptionsFunc = filterOptions || filterOptionsDefault
    const variants = filterOptionsFunc(options, value, filter)
    const hasVariants = variants && variants.length > 0

    const rootClasses = classNames(className, {
      active: open,
    })
    const dropDownClasses = classNames(classes.dropdown, {
      [dropDownClass]: open && hasVariants,
      [classes.closed]: !open || !hasVariants,
    })
    const iconClasses = classNames(classes.icon, iconClass, {
      [classes.iconOpened]: open,
      [classes.iconClosed]: !open,
    })
    const chipsClasses = classNames(classes.chips, chipsClass)
    const chipClasses = classNames(classes.chip, chipClass)
    const inputWrapperClasses = classNames(classes.inputWrapper, inputWrapperClass)

    const Icon = IconComponent || KeyboardArrowDownIcon

    let labelShrinked = false
    if (this.isMobile) {
      labelShrinked = !!value && !multi
    } else {
      labelShrinked = this.state.open || filter !== ''
    }

    const InputVariant = other && other.variant === 'outlined' ? OutlinedInput : Input

    const filterField = (
      <div className={inputWrapperClasses}>
        {label ? (
          <InputLabel shrink={labelShrinked} htmlFor={`${name}-field`} ref={this.updateLabelWidth}>
            {label}
          </InputLabel>
        ) : null}

        <InputVariant
          name={`${name}-${this.id}`}
          id={`${name}-field`}
          value={filter}
          inputRef={node => {
            this.inputRef = node
          }}
          autoComplete={`${name}-off`}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          placeholder={placeholder || (autocomplete ? `...` : '')}
          className={classes.input}
          labelWidth={labelWidth}
          {...otherInputProps}
          disabled={this.isMobile}
        />

        <IconButton className={iconClasses} onClick={this.handleIconClick}>
          <Icon className={'dropdown-icon'} />
        </IconButton>
      </div>
    )
    const DefaultItemComponent = ({ item }) => <span>{this.stringifyItem(item)}</span>
    const Item = ItemComponent || DefaultItemComponent

    return (
      <FormControl
        className={rootClasses}
        onClick={() => this.handleFocus()}
        onMouseOver={this.handleMouseOver}
        onMouseOut={this.handleMouseOut}
        {...other}
      >
        {InputComponent ? (
          <InputComponent item={value} {...ItemProps}>
            {filterField}
          </InputComponent>
        ) : (
          filterField
        )}

        <Paper
          className={dropDownClasses}
          onMouseOver={this.handleDropDownMouseOver}
          onMouseOut={this.handleDropDownMouseOut}
          {...DropDownProps}
        >
          {hasVariants ? (
            <Scrollbars
              autoHide
              autoHideTimeout={400}
              autoHideDuration={200}
              className={classes.scrollbars}
              id={`dropdown-scroll-${this.id}`}
            >
              <List id={`dropdown-content-${this.id}`}>
                {variants.map((item, key) => (
                  <ListItem
                    button
                    className={dropDownItemClass}
                    key={key}
                    onClick={event => this.handleClickItem(item)}
                  >
                    <Item item={item} {...ItemProps} />
                  </ListItem>
                ))}
              </List>
            </Scrollbars>
          ) : null}
        </Paper>

        {multi && value && Array.isArray(value) ? (
          <Grid container spacing={1} className={chipsClasses}>
            {value.map((item, key) => (
              <Chip
                label={<Item item={item} className={classes.chipItem} {...ItemProps} />}
                key={key}
                onDelete={event => this.handleRemoveItem(item)}
                className={chipClasses}
              />
            ))}
          </Grid>
        ) : null}

        {helperText ? <FormHelperText>{helperText}</FormHelperText> : null}
      </FormControl>
    )
  }
}

export default withStyles(styleSheet)(DropDownComponent)
