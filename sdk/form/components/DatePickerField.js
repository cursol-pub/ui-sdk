import { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import InfiniteCalendar from 'react-infinite-calendar'
import Input from '@material-ui/core/Input'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import IconButton from '@material-ui/core/IconButton'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import { withStyles } from '@material-ui/core/styles'

require('react-infinite-calendar/styles.css')

const styleSheet = theme => ({
  icon: {
    position: 'absolute',
    right: -4,
    top: -4,
    transition: theme.transitions.create(['transform'], {
      duration: theme.transitions.duration.shorter,
    }),
  },
  iconOpened: {
    transform: 'rotate(180deg)',
  },
  iconClosed: {
    transform: 'rotate(0deg)',
  },
  dropdown: {
    position: 'absolute',
    zIndex: 1600,
    top: 40,
    left: 0,
  },
  closed: {
    left: -5000,
  },
  inputWrapper: {
    position: 'relative',
  },
  input: {
    width: '100%',
  },
})

const stringifyItemDefault = item => (item && item instanceof Date ? item.toLocaleDateString('en-GB') : '')
const getItemValueDefault = item =>
  item && item instanceof Date
    ? `${item.getFullYear()}-${item.getMonth() < 9 ? '0' : ''}${item.getMonth() + 1}-${
        item.getDate() <= 9 ? '0' : ''
      }${item.getDate()}T00:00:00.000Z`
    : null

class DatePickerComponent extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    label: PropTypes.string,
    helperText: PropTypes.object,
    defaultValue: PropTypes.string,
    InputProps: PropTypes.object,
    DropDownProps: PropTypes.object,
    DatePickerProps: PropTypes.object,
    inputWrapperClass: PropTypes.string,
    dropDownClass: PropTypes.string,
    iconClass: PropTypes.string,
    InputComponent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    IconComponent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    stringifyItem: PropTypes.func,
    getItemValue: PropTypes.func,
  }

  state = {
    anchorEl: null,
    open: false,
    filter: '',
    syncError: '',
    labelWidth: 0,
  }

  inputRef = null
  isInputFocused = false
  isOnDropdown = false
  isMobile = false
  id = Math.floor(Math.random() * 1e10)

  constructor(props) {
    super(props)
    this.state.value = props.defaultValue && props.defaultValue !== '' ? new Date(props.defaultValue) : new Date()
  }

  componentDidMount() {
    const viewWidth = window.innerWidth || document.documentElement.clientWidth
    this.isMobile = viewWidth <= 800

    if (!this.isMobile) {
      document.addEventListener('click', this.onDocumentClick)
    }
  }

  componentWillUnmount() {
    if (!this.isMobile) {
      document.removeEventListener('click', this.onDocumentClick)
    }
  }

  updateLabelWidth = node => {
    if (!node) {
      return
    }

    const { labelWidth } = this.state
    if (labelWidth !== node.offsetWidth) {
      this.setState({
        labelWidth: node.offsetWidth,
      })
    }
  }

  onDocumentClick = e => {
    if (this.state.open && !this.isOnDropdown && !this.isInputFocused) {
      this.closeDropDown(this.state.value)
    }
  }

  handleFocus = event => {
    this.isInputFocused = true
    this.openDropDown(event)
  }

  onSelect = (date, ...other) => {
    const { value } = this.state

    if (
      value instanceof Date &&
      date instanceof Date &&
      value.getDate() === date.getDate() &&
      (value.getFullYear() !== date.getFullYear() || value.getMonth() !== date.getMonth())
    ) {
      this.setValue(date)
    } else {
      this.closeDropDown(date)
    }
  }

  handleChange = event => {
    const filter = event.target.value
    const state = { filter }

    const now = new Date()
    let matches

    matches = filter.match(/^(\d{2})[/.-](\d{2})[/.-](\d{4})$/i)
    if (
      matches &&
      parseInt(matches[1]) <= 31 &&
      parseInt(matches[2]) <= 12 &&
      parseInt(matches[3]) <= now.getFullYear()
    ) {
      state.value = new Date(parseInt(matches[3]), parseInt(matches[2]) - 1, parseInt(matches[1]))
    }

    matches = filter.match(/^(\d{2})[/.-]?(\d{2})[/.-]?(\d{2})$/i)
    if (matches && parseInt(matches[1]) <= 31 && parseInt(matches[2]) <= 12) {
      const year =
        parseInt(matches[3]) > now.getFullYear() - 2000 ? 1900 + parseInt(matches[3]) : 2000 + parseInt(matches[3])
      state.value = new Date(year, parseInt(matches[2]) - 1, parseInt(matches[1]))
    }

    matches = filter.match(/^(\d{4})[-](\d{2})[-](\d{2})$/i)
    if (
      matches &&
      parseInt(matches[1]) <= now.getFullYear() &&
      parseInt(matches[2]) <= 12 &&
      parseInt(matches[3]) <= 31
    ) {
      state.value = new Date(parseInt(matches[1]), parseInt(matches[2]) - 1, parseInt(matches[3]))
    }

    matches = filter.match(/^(\d{2})[/.-](\d{2})$/i)
    if (matches && parseInt(matches[1]) <= 31 && parseInt(matches[2]) <= 12) {
      state.value = new Date(this.state.value.getFullYear(), parseInt(matches[2]) - 1, parseInt(matches[1]))
    }

    matches = filter.match(/^(\d{4})[-](\d{2})$/i)
    if (matches && parseInt(matches[1]) <= now.getFullYear() && parseInt(matches[2]) <= 12) {
      state.value = new Date(parseInt(matches[1]), parseInt(matches[2]) - 1, now.getDay())
    }

    matches = filter.match(/^(\d{4})$/i)
    if (matches && parseInt(matches[1]) <= now.getFullYear()) {
      state.value = new Date(parseInt(matches[1]), now.getMonth(), now.getDay())
    }

    this.setState(state)
  }

  handleChangeMobile = event => {
    this.setValue(new Date(event.target.value), event.target.value)
  }

  handleBlur = () => {
    this.isInputFocused = false
    if (!this.isOnDropdown && !this.isMobile) {
      this.closeDropDown(this.state.value)
    }
  }

  handleIconClick = () => {
    if (this.state.open) {
      this.closeDropDown(this.state.value)
    } else {
      this.focusFilter()
    }
  }

  handleDropDownMouseOver = event => {
    this.isOnDropdown = true
  }

  handleDropDownMouseOut = event => {
    this.isOnDropdown = false
  }

  focusFilter = () => {
    if (this.inputRef) {
      this.inputRef.focus()
    }
  }

  openDropDown = event => {
    if (!this.state.open) {
      this.setState({ open: true, anchorEl: event.currentTarget, filter: '' })
    }
  }

  closeDropDown(value) {
    this.isOnDropdown = false
    this.setValue(value)

    this.setState({
      open: false,
    })
  }

  setValue(value, filter = null) {
    const {
      InputProps: { onChange },
      DatePickerProps,
    } = this.props

    if (DatePickerProps && DatePickerProps.maxDate && value.getTime() > DatePickerProps.maxDate.getTime()) {
      this.setState({
        filter: '',
        syncError: `max date is ${this.stringifyItem(DatePickerProps.maxDate)}`,
      })
      return
    }

    if (onChange) {
      onChange(this.getItemValue(value))
    }

    this.setState({
      filter: filter || this.stringifyItem(value),
      value,
      syncError: '',
    })
  }

  getItemValue = value => {
    const { getItemValue } = this.props
    return getItemValue ? getItemValue(value) : getItemValueDefault(value)
  }

  stringifyItem = value => {
    const { stringifyItem } = this.props
    return stringifyItem ? stringifyItem(value) : stringifyItemDefault(value)
  }

  renderMobile = () => {
    const {
      classes,
      className,
      inputWrapperClass,
      dropDownClass,
      iconClass,
      label,
      helperText,
      InputProps,
      DropDownProps,
      DatePickerProps,
      InputComponent,
      IconComponent,
      ...other
    } = this.props
    const { name, value: inputValue, onFocus, onBlur, onChange, ...otherInputProps } = InputProps
    const { filter, value, syncError, labelWidth } = this.state

    const rootClasses = classNames(className, {
      error: syncError !== '',
    })
    const inputWrapperClasses = classNames(classes.inputWrapper, inputWrapperClass, {
      error: syncError !== '',
    })

    const InputVariant = other && other.variant === 'outlined' ? OutlinedInput : Input

    const filterField = (
      <div className={inputWrapperClasses}>
        {label ? (
          <InputLabel shrink={this.state.open || filter !== ''} ref={this.updateLabelWidth}>
            {label}
          </InputLabel>
        ) : null}

        <InputVariant
          name={name}
          type={'date'}
          value={filter}
          inputRef={node => {
            this.inputRef = node
          }}
          autoComplete={`${name}-off`}
          onFocus={this.handleFocus}
          onChange={this.handleChangeMobile}
          onBlur={this.handleBlur}
          placeholder={''}
          className={classes.input}
          labelWidth={labelWidth}
          {...otherInputProps}
        />
      </div>
    )

    return (
      <FormControl className={rootClasses} onClick={this.focusFilter} {...other}>
        {InputComponent ? (
          <InputComponent item={value} {...InputProps}>
            {filterField}
          </InputComponent>
        ) : (
          filterField
        )}

        {helperText || syncError !== '' ? (
          <FormHelperText>{syncError !== '' ? syncError : helperText}</FormHelperText>
        ) : null}
      </FormControl>
    )
  }

  render() {
    if (this.isMobile) {
      return this.renderMobile()
    }

    const {
      classes,
      className,
      inputWrapperClass,
      dropDownClass,
      iconClass,
      label,
      helperText,
      InputProps,
      DropDownProps,
      DatePickerProps,
      InputComponent,
      IconComponent,
      getItemValue,
      stringifyItem,
      ...other
    } = this.props
    const { name, value: inputValue, onFocus, onBlur, onChange, ...otherInputProps } = InputProps
    const { open, filter, value, syncError, labelWidth } = this.state

    const rootClasses = classNames(className, {
      error: syncError !== '',
      active: open,
    })
    const dropDownClasses = classNames(classes.dropdown, {
      [dropDownClass]: open,
      [classes.closed]: !open,
    })
    const iconClasses = classNames(classes.icon, iconClass, {
      [classes.iconOpened]: open,
      [classes.iconClosed]: !open,
    })
    const inputWrapperClasses = classNames(classes.inputWrapper, inputWrapperClass, {
      error: syncError !== '',
    })

    const Icon = IconComponent || KeyboardArrowDownIcon
    const InputVariant = other && other.variant === 'outlined' ? OutlinedInput : Input

    const filterField = (
      <div className={inputWrapperClasses}>
        {label ? (
          <InputLabel shrink={this.state.open || filter !== ''} ref={this.updateLabelWidth}>
            {label}
          </InputLabel>
        ) : null}

        <InputVariant
          name={`${name}-${this.id}`}
          value={filter}
          inputRef={node => {
            this.inputRef = node
          }}
          autoComplete={'off'}
          onFocus={this.handleFocus}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          placeholder={''}
          className={classes.input}
          labelWidth={labelWidth}
          {...otherInputProps}
        />

        <IconButton
          className={iconClasses}
          onClick={this.handleIconClick}
          onMouseOver={this.handleDropDownMouseOver}
          onMouseOut={this.handleDropDownMouseOut}
        >
          <Icon />
        </IconButton>
      </div>
    )

    return (
      <FormControl
        className={rootClasses}
        onMouseOver={this.handleDropDownMouseOver}
        onMouseOut={this.handleDropDownMouseOut}
        onClick={this.openDropDown}
        {...other}
      >
        {InputComponent ? (
          <InputComponent item={value} {...InputProps}>
            {filterField}
          </InputComponent>
        ) : (
          filterField
        )}

        <div
          className={dropDownClasses}
          onMouseOver={this.handleDropDownMouseOver}
          onMouseOut={this.handleDropDownMouseOut}
          {...DropDownProps}
        >
          <InfiniteCalendar
            width={400}
            height={500}
            onSelect={this.onSelect}
            autoFocus={false}
            selected={value}
            {...DatePickerProps}
          />
        </div>
        {helperText || syncError !== '' ? (
          <FormHelperText>{syncError !== '' ? syncError : helperText}</FormHelperText>
        ) : null}
      </FormControl>
    )
  }
}

export default withStyles(styleSheet)(DatePickerComponent)
