import PropTypes from 'prop-types'
import Flag from 'react-country-flag'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'

import DropDownComponent from './DropDownComponent'

const styles = theme => ({
  wrapper: {
    display: 'flex',
    width: '100%',
    flexWrap: 'nowrap',
    flexDirection: 'row',
  },
  flag: {
    width: 28,
    maxHeight: 16,
    position: 'absolute',
    top: '50%',
    marginTop: -8,
  },
  flagCont: {
    flex: '0 0 40px',
    position: 'relative',
  },
  labelCont: {
    flex: '1 1 100%',
    position: 'relative',
  },
})
const ItemComponent = props => {
  const { classes, itemFlagContClass, item, stringifyFlagItem, className } = props
  const rootClasses = classNames(classes.wrapper, className)
  const flagContClasses = classNames(classes.flagCont, itemFlagContClass)

  return (
    <div className={rootClasses}>
      <div className={flagContClasses}>
        {item && item.country !== '' ? <Flag svg code={item.country} className={classes.flag} /> : null}
      </div>

      <div className={classes.labelCont}>{stringifyFlagItem ? stringifyFlagItem(item) : item.label}</div>
    </div>
  )
}
ItemComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  itemFlagContClass: PropTypes.string,
  item: PropTypes.object,
  stringifyFlagItem: PropTypes.func,
}
const StyledItemComponent = withStyles(styles)(ItemComponent)

const InputComponent = props => {
  const { classes, inputFlagContClass, item, children, className } = props
  const rootClasses = classNames(classes.wrapper, className)
  const flagContClasses = classNames(classes.flagCont, inputFlagContClass)

  return (
    <div className={rootClasses}>
      <div className={flagContClasses}>
        {item && item.country !== '' ? <Flag svg code={item.country} className={classes.flag} /> : null}
      </div>

      <div className={classes.labelCont}>{children}</div>
    </div>
  )
}
InputComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  inputFlagContClass: PropTypes.string,
  item: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
}
const StyledInputComponent = withStyles(styles)(InputComponent)

const FlagsDropDownField = props => {
  const { inputFlagContClass, itemFlagContClass, stringifyFlagItem, ...other } = props
  return (
    <DropDownComponent
      ItemComponent={StyledItemComponent}
      InputComponent={StyledInputComponent}
      ItemProps={{
        inputFlagContClass,
        itemFlagContClass,
        stringifyFlagItem,
      }}
      {...other}
    />
  )
}
FlagsDropDownField.propTypes = {
  inputFlagContClass: PropTypes.string,
  itemFlagContClass: PropTypes.string,
  stringifyFlagItem: PropTypes.func,
}

export default FlagsDropDownField
