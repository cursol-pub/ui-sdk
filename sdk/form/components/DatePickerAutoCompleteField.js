import { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import Input from '@material-ui/core/Input'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  input: {
    width: '100%',
  },
})

const stringifyItemDefault = item => (item && item instanceof Date ? item.toLocaleDateString('en-GB') : '')
const getItemValueDefault = item =>
  item && item instanceof Date
    ? `${item.getFullYear()}-${item.getMonth() < 9 ? '0' : ''}${item.getMonth() + 1}-${
        item.getDate() <= 9 ? '0' : ''
      }${item.getDate()}T00:00:00.000Z`
    : null

class DatePickerAutoCompleteComponent extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    label: PropTypes.string,
    helperText: PropTypes.object,
    InputProps: PropTypes.object,
    inputWrapperClass: PropTypes.string,
    InputComponent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    stringifyItem: PropTypes.func,
    getItemValue: PropTypes.func,
  }

  state = {
    open: false,
    filter: '',
    syncError: '',
    labelWidth: 0,
    value: null,
  }

  inputRef = null
  isInputFocused = false
  isOnDropdown = false
  isMobile = false
  id = Math.floor(Math.random() * 1e10)

  componentDidMount() {
    if (!this.props.InputProps) {
      return
    }
    const { value: inpValue } = this.props.InputProps

    if (inpValue && inpValue !== '') {
      try {
        const value = new Date(inpValue)
        const filter = this.stringifyItem(value)
        this.setState({
          value,
          filter,
        })
      } catch (err) {
        console.log(err)
      }
    }
  }

  updateLabelWidth = node => {
    if (!node) {
      return
    }

    const { labelWidth } = this.state
    if (labelWidth !== node.offsetWidth) {
      this.setState({
        labelWidth: node.offsetWidth,
      })
    }
  }

  handleFocus = event => {
    this.setState({
      open: true,
      filter: '',
    })
  }

  handleChange = event => {
    const filter = event.target.value
    const state = { filter }

    const now = new Date()
    let matches

    matches = filter.match(/(\d{2})\/(\d{2})\/(\d{4})/i)
    if (matches && parseInt(matches[1]) <= 31 && parseInt(matches[2]) <= 12) {
      state.value = new Date(parseInt(matches[3]), parseInt(matches[2]) - 1, parseInt(matches[1]))
    }

    matches = filter.match(/(\d{2})\/(\d{2})\/(\d{2})\s*$/i)
    if (matches && parseInt(matches[1]) <= 31 && parseInt(matches[2]) <= 12) {
      const year =
        parseInt(matches[3]) > now.getFullYear() - 2000 ? 1900 + parseInt(matches[3]) : 2000 + parseInt(matches[3])
      state.value = new Date(year, parseInt(matches[2]) - 1, parseInt(matches[1]))
    }

    matches = filter.match(/^\s*(\d)[./\s]$/i)
    if (matches) {
      state.filter = `0${matches[1]}/`
    }

    matches = filter.match(/^\s*(\d{2})[.\s]$/i)
    if (matches) {
      state.filter = `${matches[1]}/`
    }

    matches = filter.match(/^(\d{2})(\d)$/i)
    if (matches) {
      state.filter = `${matches[1]}/${matches[2]}`
    }

    matches = filter.match(/^\s*(\d{2})\/[^\d]+$/i)
    if (matches) {
      state.filter = `${matches[1]}/`
    }

    matches = filter.match(/^(\d{2})\/(\d)[./\s]$/i)
    if (matches) {
      state.filter = `${matches[1]}/0${matches[2]}/`
    }

    matches = filter.match(/^(\d{2})\/(\d{2})[.\s]$/i)
    if (matches) {
      state.filter = `${matches[1]}/${matches[2]}/`
    }

    matches = filter.match(/^(\d{2})\/(\d{2})\/[^\d]+$/i)
    if (matches) {
      state.filter = `${matches[1]}/${matches[2]}/`
    }

    matches = filter.match(/^(\d{2})\/(\d{2})(\d)$/i)
    if (matches) {
      state.filter = `${matches[1]}/${matches[2]}/${matches[3]}`
    }

    this.setState(state)
  }

  handleBlur = () => {
    this.setValue(this.state.value)
  }

  focusFilter = () => {
    if (this.inputRef) {
      this.inputRef.focus()
    }
  }

  setValue(value, filter = null) {
    const {
      InputProps: { onChange },
      DatePickerProps,
    } = this.props

    if (DatePickerProps && DatePickerProps.maxDate && value && value.getTime() > DatePickerProps.maxDate.getTime()) {
      this.setState({
        filter: '',
        syncError: `max date is ${this.stringifyItem(DatePickerProps.maxDate)}`,
      })
      return
    }

    if (onChange) {
      onChange(this.getItemValue(value))
    }

    this.setState({
      open: false,
      filter: filter || this.stringifyItem(value),
      value,
      syncError: '',
    })
  }

  getItemValue = value => {
    const { getItemValue } = this.props
    return getItemValue ? getItemValue(value) : getItemValueDefault(value)
  }

  stringifyItem = value => {
    const { stringifyItem } = this.props
    return stringifyItem ? stringifyItem(value) : stringifyItemDefault(value)
  }

  render() {
    const {
      classes,
      className,
      inputWrapperClass,
      dropDownClass,
      iconClass,
      label,
      helperText,
      InputProps,
      DropDownProps,
      DatePickerProps,
      InputComponent,
      IconComponent,
      ...other
    } = this.props
    const { name, value: inputValue, onFocus, onBlur, onChange, ...otherInputProps } = InputProps
    const { open, filter, value, syncError, labelWidth } = this.state

    const rootClasses = classNames(className, {
      error: syncError !== '',
    })
    const inputWrapperClasses = classNames(inputWrapperClass, {
      error: syncError !== '',
    })

    const InputVariant = other && other.variant === 'outlined' ? OutlinedInput : Input

    const filterField = (
      <div className={inputWrapperClasses}>
        {label ? (
          <InputLabel shrink={open || filter !== ''} ref={this.updateLabelWidth}>
            {label}
          </InputLabel>
        ) : null}

        <InputVariant
          name={`${name}-${this.id}`}
          value={filter}
          inputRef={node => {
            this.inputRef = node
          }}
          autoComplete={`${name}-off`}
          onFocus={this.handleFocus}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          placeholder={'Date of Birth (dd/mm/yyyy)'}
          className={classes.input}
          labelWidth={labelWidth}
          {...otherInputProps}
        />
      </div>
    )

    return (
      <FormControl className={rootClasses} onClick={this.focusFilter} {...other}>
        {InputComponent ? (
          <InputComponent item={value} {...InputProps}>
            {filterField}
          </InputComponent>
        ) : (
          filterField
        )}

        {helperText || syncError !== '' ? (
          <FormHelperText>{syncError !== '' ? syncError : helperText}</FormHelperText>
        ) : null}
      </FormControl>
    )
  }
}

export default withStyles(styleSheet)(DatePickerAutoCompleteComponent)
