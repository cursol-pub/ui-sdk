import { Component } from 'react'
import PropTypes from 'prop-types'

import DropDownComponent from './DropDownComponent'
import { sortByProperty } from './../../utils'

class DropDownFieldComponent extends Component {
  static propTypes = {
    options: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    sort: PropTypes.bool,
  }

  parseOptions(options) {
    const { sort } = this.props

    let parsed = []
    if (Array.isArray(options)) {
      parsed = options.map((val, index) => ({
        value: val + '',
        label: val + '',
      }))
    } else {
      parsed = Object.keys(options).map(value => ({
        value,
        label: options[value] + '',
      }))
    }

    return !sort ? parsed : parsed.sort(sortByProperty('label'))
  }

  render() {
    const { options, sort, ...other } = this.props

    return <DropDownComponent options={this.parseOptions(options)} {...other} />
  }
}

export default DropDownFieldComponent
