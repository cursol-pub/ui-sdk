import { Component } from 'react'
import PropTypes from 'prop-types'

import FormControl from '@material-ui/core/FormControl'
import Checkbox from '@material-ui/core/Checkbox'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  item: {
    height: 'auto',
  },
})

class CheckboxFieldComponent extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.func]),
    helperText: PropTypes.object,
    InputProps: PropTypes.object.isRequired,
    disabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  }

  handleChange = (event, checked) => {
    const {
      InputProps: { onChange },
    } = this.props
    if (onChange) {
      onChange(checked)
    }
  }

  render() {
    const { classes, label, InputProps, options, helperText, ...other } = this.props
    const { name, value } = InputProps

    return (
      <FormControl component="fieldset" {...other}>
        <FormControlLabel
          control={<Checkbox checked={!!value} onChange={this.handleChange} value={name} />}
          label={label}
          className={classes.item}
        />

        {helperText ? <FormHelperText>{helperText}</FormHelperText> : null}
      </FormControl>
    )
  }
}

export default withStyles(styleSheet)(CheckboxFieldComponent)
