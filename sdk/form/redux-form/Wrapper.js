import PropTypes from 'prop-types'
import classNames from 'classnames'

const id = Math.floor(Math.random() * 1e10)

const FieldWrapper = props => {
  const { input, meta, fieldError, component: TextFieldComponent, className, showOnTheFlyErrors, ...otherProps } = props
  let { error, helperText, ...other } = otherProps

  if (fieldError && fieldError.message !== '') {
    error = true
    helperText = <span>{fieldError.message}</span>
  }

  if (meta.touched && (!fieldError || input.value !== fieldError.value)) {
    error = typeof meta.error !== 'undefined' || typeof meta.submitError !== 'undefined'
    helperText = showOnTheFlyErrors && error ? <span>{meta.error || meta.submitError}</span> : helperText
  }

  const rootClasses = classNames(className, {
    active: meta.active,
    error: error,
    success: meta.dirty && !error,
  })

  input['name'] = `${input.name}-${id}`

  return (
    <TextFieldComponent
      fullWidth
      InputProps={input}
      error={error}
      helperText={helperText}
      className={rootClasses}
      margin="dense"
      variant="outlined"
      {...other}
    />
  )
}
FieldWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  error: PropTypes.bool,
  showOnTheFlyErrors: PropTypes.bool,
  helperText: PropTypes.string,
  fieldError: PropTypes.object,
  component: PropTypes.any.isRequired,
}
FieldWrapper.defaultProps = {
  showOnTheFlyErrors: true,
}

export default FieldWrapper
