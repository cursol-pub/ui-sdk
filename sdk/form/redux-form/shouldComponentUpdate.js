const shouldComponentUpdate = (props, nextProps, propsList = []) => {
  return propsList.reduce((result, propName) => {
    if (typeof props[propName] === 'undefined' && typeof nextProps[propName] !== 'undefined') {
      return true
    }

    if (typeof props[propName] !== 'undefined' && typeof nextProps[propName] === 'undefined') {
      return true
    }

    if (
      typeof props[propName] === 'object' &&
      JSON.stringify(props[propName]) !== JSON.stringify(nextProps[propName])
    ) {
      return true
    }

    if (
      ['boolean', 'string', 'number'].indexOf(typeof props[propName]) > -1 &&
      props[propName] !== nextProps[propName]
    ) {
      return true
    }

    return result
  }, false)
}
export default shouldComponentUpdate
