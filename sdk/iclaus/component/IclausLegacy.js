import { onMessageHandler } from './../../postm'
import { setIclaus, onIclausReady, onMessage } from './..'

const styles = 'position: absolute; opacity: 0; border: 0px; width: 1px; height: 1px; bottom: 0px'

export default (url, origins) => {
  onMessageHandler(onMessage(origins))

  const n = document.createElement('iframe')
  n.id = 'iclaus'
  n.style = styles
  n.src = url
  n.onload = onIclausReady
  document.getElementsByTagName('body')[0].appendChild(n)

  setIclaus(n.contentWindow)
  setTimeout(() => {
    onIclausReady()
  }, 500)
}
