import { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { onMessageHandler, onMessageHandlerRemove } from './../../postm'
import { setIclaus, onIclausReady, onMessage } from './..'

const styleSheet = theme => ({
  root: {
    position: 'absolute',
    opacity: 0,
    border: 0,
    width: 1,
    height: 1,
  },
})

class IclausComponent extends Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    origins: PropTypes.array.isRequired,
  }

  componentDidMount() {
    const { origins } = this.props
    onMessageHandler(onMessage(origins))
  }

  componentWillUnmount() {
    const { origins } = this.props
    onMessageHandlerRemove(onMessage(origins))
  }

  render() {
    const { classes, url } = this.props

    return (
      <iframe
        src={url}
        className={classes.root}
        ref={node => {
          if (node) {
            setIclaus(node.contentWindow)
            setTimeout(() => {
              onIclausReady()
            }, 5000)
          }
        }}
        onLoad={() =>
          setTimeout(() => {
            onIclausReady()
          }, 500)
        }
      />
    )
  }
}

export default withStyles(styleSheet)(IclausComponent)
