import 'core-js/stable'

import AppConfig from 'AppConfig'
let iclaus
let isReady = false
let onReadyQueue = []
const resolvers = {}

export const setIclaus = node => {
  localStorage && localStorage.csdebug && console.log('SDK: set iClaus iframe', node)
  iclaus = node
}

export const onIclausReady = () => {
  isReady = true
  localStorage && localStorage.csdebug && console.log('SDK: iClaus ready')

  if (onReadyQueue.length) {
    onReadyQueue.map(({ action, data: body, reqId: requestId }) => {
      localStorage &&
        localStorage.csdebug &&
        console.log('SDK: post message sent to iClaus', body, iclaus, AppConfig.claus.hostname)
      iclaus.postMessage(
        {
          action,
          body,
          requestId,
        },
        AppConfig.claus.hostname
      )
    })
    onReadyQueue = []
  }
}

export const postMessage = (action, data) => {
  localStorage && localStorage.csdebug && console.log('SDK: post message to iClaus', data)
  return new Promise((resolve, reject) => {
    const reqId = `pm-${Math.floor(Math.random() * 1e10)}-${Math.floor(Math.random() * 1e16)}`
    resolvers[reqId] = data => resolve(data)

    if (isReady) {
      localStorage && localStorage.csdebug && console.log('SDK: post message sent to iClaus', data)
      iclaus.postMessage(
        {
          action,
          body: data,
          requestId: reqId,
        },
        AppConfig.claus.hostname
      )
    } else {
      localStorage && localStorage.csdebug && console.log('SDK: post message added to queue', data)
      onReadyQueue.push({
        action,
        data,
        reqId,
      })
    }
  })
}

export const resolveReq = (reqId, data) => {
  if (resolvers[reqId] && typeof resolvers[reqId] === 'function') {
    resolvers[reqId](data)
    delete resolvers[reqId]
  }
}

export const onMessage = origins => event => {
  localStorage && localStorage.csdebug && console.log('SDK: on message', event)
  if (!event || !event.origin) {
    localStorage && localStorage.csdebug && console.error('SDK: Wrong response Post Message', event)
    return
  }

  if (Array.isArray(origins) && origins.length && origins.indexOf(event.origin) === -1) {
    localStorage && localStorage.csdebug && console.log('SDK: not allowed origin', origins, event.origin)
    return
  }

  const { data } = event
  if (!data || !data.action || data.action !== 'response' || !data.requestId) {
    return
  }

  resolveReq(data.requestId, data.body)
}
