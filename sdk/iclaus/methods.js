import { postMessage } from './'
import { hasToken, setToken, isTokenExpired } from './../auth/jwt'
import { getRequest } from './../rest/requests'

let getTokenPromise = null

export const getToken = payload => {
  const payloadToSend = Object.assign(
    {
      query: window.location.search,
      referrer: document.referrer,
    },
    payload
  )
  return postMessage('token.get', payloadToSend)
}

export const refreshToken = () => postMessage('token.refresh')

export const signOut = () => postMessage('auth.signout')

export const clausTokenRequiredRequest = (getProjectTokenUrl, clausPayload) => (handler, ...params) => {
  localStorage && localStorage.csdebug && console.log('iClaus required request: ', params)
  if (hasToken() && !isTokenExpired()) {
    localStorage &&
      localStorage.csdebug &&
      console.log('iClaus required request: already has token, fire handler', params)
    return handler(...params)
  }

  if (!getTokenPromise) {
    localStorage && localStorage.csdebug && console.log('iClaus required request: no token promise, create one')
    getTokenPromise = getToken(clausPayload).then(clausData => {
      localStorage &&
        localStorage.csdebug &&
        console.log('iClaus required request: claus token data arrived', clausData)
      if (!clausData.token) {
        throw new Error('claus token required')
      }

      return getRequest(getProjectTokenUrl, { Authorization: clausData.token }).then(data => {
        localStorage && localStorage.csdebug && console.log('iClaus required request: project token data arrived', data)
        getTokenPromise = null
        if (!data.token) {
          throw new Error('token required')
        }

        setToken(data.token)
      })
    })
  }

  return getTokenPromise.then(() => {
    localStorage &&
      localStorage.csdebug &&
      console.log('iClaus required request: token promise resolved, fire handler', params)
    return handler(...params)
  })
}
