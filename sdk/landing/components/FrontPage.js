import { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import ButtonBase from '@material-ui/core/ButtonBase'

import { withStyles } from '@material-ui/core/styles'

import Auth from './../../auth/containers/Auth'
import ContentBlock from './../../layout/ContentBlock'

const styleSheet = theme => ({
  title: {
    fontSize: '24px',
    fontWeight: 300,
    textTransform: 'capitalize',
  },
  icon: {
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    height: 120,
    width: '100%',
  },
  button: {
    width: '100%',
    height: '100%',
  },
  iconContainer: {
    padding: `${theme.spacing(2)}px !important`,
  },
  textContainer: {
    display: 'flex',
    alignItems: 'center',
  },
})

class FrontPageContainer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    error: PropTypes.object,
    bookmarks: PropTypes.array.isRequired,
  }

  render() {
    const { classes, bookmarks } = this.props
    return (
      <Auth user redirect>
        <ContentBlock lg flatten>
          <Grid container spacing={2}>
            {bookmarks.map(bookmark => {
              const buttonProps =
                bookmark.link && bookmark.link !== ''
                  ? {
                      component: Link,
                      to: bookmark.link,
                    }
                  : {
                      href: bookmark.url,
                    }
              return (
                <Grid item xs={12} md={4} key={bookmark.code}>
                  <Paper>
                    <ButtonBase focusRipple className={classes.button} {...buttonProps}>
                      <Grid container spacing={1} justify="flex-start" alignItems="center">
                        <Grid item xs={4} className={classes.iconContainer}>
                          <div style={{ backgroundImage: 'url(' + bookmark.icon + ')' }} className={classes.icon} />
                        </Grid>
                        <Grid item xs={8} className={classes.textContainer}>
                          <Typography className={classes.title}>{bookmark.name}</Typography>
                        </Grid>
                      </Grid>
                    </ButtonBase>
                  </Paper>
                </Grid>
              )
            })}
          </Grid>
        </ContentBlock>
      </Auth>
    )
  }
}

export default withStyles(styleSheet)(FrontPageContainer)
