import PropTypes from 'prop-types'
import classNames from 'classnames'

import { makeStyles } from '@material-ui/styles'
import Divider from '@material-ui/core/Divider'

const useStyles = makeStyles(theme => ({
  root: {
    height: 2,
  },
  primary: {
    backgroundColor: theme.palette.primary.main,
  },
  secondary: {
    backgroundColor: theme.palette.secondary.main,
  },
}))

const StyledDivider = ({ color, ...props }) => {
  const classes = useStyles()

  return (
    <Divider
      classes={{
        root: classNames(classes.root, {
          [classes.primary]: color === 'primary',
          [classes.secondary]: color === 'secondary',
        }),
      }}
      {...props}
    />
  )
}

StyledDivider.propTypes = {
  color: PropTypes.oneOf(['default', 'primary', 'secondary']),
}

StyledDivider.defaultProps = {
  color: 'secondary',
}

export default StyledDivider
