import PropTypes from 'prop-types'
import classNames from 'classnames'

import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Fade from '@material-ui/core/Fade'
import Grid from '@material-ui/core/Grid'

const styleSheet = theme => {
  let styles = {
    root: {
      flex: '1 1 100%',
      maxWidth: '100%',
      margin: '0 auto',
      padding: theme.spacing(1),
      [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(2),
      },
    },
    stretched: {
      display: 'flex',
      height: '100%',
    },
  }

  styles = theme.breakpoints.keys.reduce((styles, breakpoint) => {
    styles[`width-${breakpoint}`] = {
      [theme.breakpoints.up(breakpoint)]: {
        maxWidth: theme.breakpoints.values[breakpoint],
      },
    }
    return styles
  }, styles)

  return styles
}

const ContentBlock = ({ children, classes, flatten, xs, sm, md, lg, xl, stretch }) => {
  const className = classNames(classes.root, {
    [classes['width-xs']]: xs,
    [classes['width-sm']]: sm,
    [classes['width-md']]: md,
    [classes['width-lg']]: lg,
    [classes['width-xl']]: xl,
    [classes.stretched]: stretch,
  })

  return (
    <Fade in timeout={{ enter: 350, exit: 200 }}>
      {flatten ? (
        <Grid container spacing={1} className={className}>
          <Grid item xs={12}>
            {children}
          </Grid>
        </Grid>
      ) : (
        <Paper className={className}>{children}</Paper>
      )}
    </Fade>
  )
}

ContentBlock.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  classes: PropTypes.object.isRequired,
  flatten: PropTypes.bool,
  xs: PropTypes.bool,
  sm: PropTypes.bool,
  md: PropTypes.bool,
  lg: PropTypes.bool,
  xl: PropTypes.bool,
  stretch: PropTypes.bool,
}

ContentBlock.defaultProps = {
  flatten: false,
  xs: false,
  sm: false,
  md: false,
  lg: false,
  xl: false,
  stretch: false,
}

export default withStyles(styleSheet)(ContentBlock)
