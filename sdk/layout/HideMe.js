import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  hide: {
    display: 'none',
  },
})

const HideMe = ({ children, classes, show }) => {
  const props = !show ? { className: classes.hide } : {}

  return <div {...props}>{children}</div>
}

HideMe.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  classes: PropTypes.object.isRequired,
  show: PropTypes.bool,
}

HideMe.defaultProps = {
  show: false,
}

export default withStyles(styleSheet)(HideMe)
