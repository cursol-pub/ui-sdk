import PropTypes from 'prop-types'
import classNames from 'classnames'

import { makeStyles } from '@material-ui/styles'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles(theme => ({
  root: {
    background: '#00ACC1',
    color: 'white',
    minWidth: 0,
    '&:hover': {
      background: '#028d9e',
    },
  },
  inline: {
    padding: 4,
    borderRadius: 5,
    margin: '0 2px',
  },
  block: {
    padding: theme.spacing(1),
    borderRadius: 20,
  },
  hoverable: {
    opacity: 0,
    transition: 'opacity .4s ease',
  },
  inlineIcon: {
    width: 18,
    height: 18,
  },
}))

const IconButton = ({ icon: IconComponent, inline, hoverable, ...props }) => {
  const classes = useStyles()

  return (
    <Button
      variant="contained"
      color="secondary"
      classes={{
        root: classNames(classes.root, {
          [classes.inline]: inline,
          [classes.block]: !inline,
          [classes.hoverable]: hoverable,
        }),
      }}
      {...props}
    >
      <IconComponent
        classes={{
          root: classNames('', {
            [classes.inlineIcon]: inline,
          }),
        }}
      />
    </Button>
  )
}

IconButton.propTypes = {
  icon: PropTypes.any.isRequired,
  inline: PropTypes.bool,
  hoverable: PropTypes.bool,
}

IconButton.defaultProps = {
  inline: false,
  hoverable: false,
}

export default IconButton
