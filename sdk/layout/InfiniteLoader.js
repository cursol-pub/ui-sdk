import { Component } from 'react'
import PropTypes from 'prop-types'
import { Scrollbars } from 'react-custom-scrollbars'
import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  root: {
    height: 'auto !important',
  },
})

class InfiniteLoad extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    scrollHandler: PropTypes.func.isRequired,
    isEnabled: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    bottomOffset: PropTypes.number.isRequired,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  }

  triggerScrollHandler = values => {
    const { isEnabled, isLoading, bottomOffset, scrollHandler } = this.props
    const documentBottomVisibleOffset = values.scrollHeight - values.scrollHeight * values.top

    if (isEnabled && !isLoading && documentBottomVisibleOffset <= bottomOffset) {
      scrollHandler()
    }
  }

  render() {
    const { classes, children } = this.props

    return (
      <Scrollbars
        autoHide
        autoHideTimeout={400}
        autoHideDuration={200}
        onUpdate={this.triggerScrollHandler}
        className={classes.root}
      >
        {children}
      </Scrollbars>
    )
  }
}

export default withStyles(styleSheet)(InfiniteLoad)
