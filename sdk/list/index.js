const defaultList = {}

export const prepareList = (lists, listName) => {
  if (!lists || !lists[listName]) {
    return defaultList
  }

  return lists[listName]
}
