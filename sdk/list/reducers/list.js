import {
  LIST_GET_NAMES,
  LIST_GET_NAMES_SUCCESS,
  LIST_GET_NAMES_FAIL,
  LIST_GET_BY_NAME,
  LIST_GET_BY_NAME_SUCCESS,
  LIST_GET_BY_NAME_FAIL,
} from './../actions/list'

const initialState = {
  isLoading: false,
  lists: null,
  listNames: [],
  error: null,
}

const reducers = (state = initialState, actionData = null) => {
  switch (actionData.type) {
    case LIST_GET_NAMES:
      return {
        ...state,
        isLoading: true,
        listNames: [],
      }

    case LIST_GET_NAMES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        listNames: actionData.data.list,
      }

    case LIST_GET_NAMES_FAIL:
      return {
        ...state,
        isLoading: false,
        error: actionData.error,
      }

    case LIST_GET_BY_NAME:
      return {
        ...state,
        isLoading: true,
        lists: {
          ...state.lists,
          [actionData.name]: {},
        },
      }

    case LIST_GET_BY_NAME_SUCCESS:
      let listData = actionData.data.list || {}

      if (typeof actionData.hydrator === 'function') {
        listData = actionData.assoc ? listData.reduce(actionData.hydrator, {}) : listData.map(actionData.hydrator)
      }

      return {
        ...state,
        isLoading: false,
        error: null,
        lists: {
          ...state.lists,
          [actionData.name]: listData,
        },
      }

    case LIST_GET_BY_NAME_FAIL:
      return {
        ...state,
        isLoading: false,
        error: actionData.error,
      }

    default:
      return state
  }
}

export default reducers
