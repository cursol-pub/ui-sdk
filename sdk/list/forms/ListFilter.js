import { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'

import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import SearchIcon from '@material-ui/icons/Search'

import { isRequired } from './../../form/Field/validation'
import ButtonIcon from './../../icon/ButtonIcon'
import DropDown from './../../form/Field/DropDown'
import shouldComponentUpdate from './../../form/redux-form/shouldComponentUpdate'

class ListFilterForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    validationErrors: PropTypes.object,
    initialValues: PropTypes.object,
    listNames: PropTypes.array,
  }

  shouldComponentUpdate(nextProps) {
    return shouldComponentUpdate(this.props, nextProps, ['isSubmitting', 'validationErrors', 'listNames'])
  }

  prepareOptions = list =>
    list.reduce((result, item) => {
      return {
        ...result,
        [item]: item,
      }
    }, {})

  render() {
    const { handleSubmit, isSubmitting, validationErrors, listNames } = this.props

    return (
      <form onSubmit={handleSubmit}>
        <Grid container spacing={1} justify="flex-start" alignItems="center">
          <Grid item>
            <Field
              component={DropDown}
              label="Choose list"
              name="list"
              options={this.prepareOptions(listNames)}
              required
              validate={isRequired('List')}
              fieldError={validationErrors.list}
            />
          </Grid>

          <Grid item>
            <Button variant="contained" color="primary" type="submit" disabled={isSubmitting}>
              Show <ButtonIcon component={SearchIcon} />
            </Button>
          </Grid>
        </Grid>
      </form>
    )
  }
}

export default reduxForm({
  form: 'listFilter',
})(ListFilterForm)
