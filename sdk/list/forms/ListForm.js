import { Component } from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'

import Typography from '@material-ui/core/Typography'
import Divider from './../../layout/Divider'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'

import CircularProgress from '@material-ui/core/CircularProgress'
import TextField from './../../form/Field/TextField'
import { isRequired } from './../../form/Field/validation'
import ContentBlock from './../../layout/ContentBlock'
import ButtonIcon from './../../icon/ButtonIcon'
import DropDown from './../../form/Field/DropDown'
import TextAutoComplete from './../../form/Field/TextAutoComplete'
import shouldComponentUpdate from './../../form/redux-form/shouldComponentUpdate'

class ListForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    isLoadingItem: PropTypes.bool,
    validationErrors: PropTypes.object,
    initialValues: PropTypes.object,
    listNames: PropTypes.array,
  }

  shouldComponentUpdate(nextProps) {
    return shouldComponentUpdate(this.props, nextProps, [
      'isSubmitting',
      'isLoadingItem',
      'validationErrors',
      'initialValues',
      'listNames',
    ])
  }

  prepareOptions = list =>
    list.reduce((result, item) => {
      return {
        ...result,
        [item]: item,
      }
    }, {})

  render() {
    const { handleSubmit, isSubmitting, isLoadingItem, validationErrors, initialValues, listNames } = this.props

    if (isLoadingItem) {
      return (
        <ContentBlock sm>
          <CircularProgress />
        </ContentBlock>
      )
    }

    return (
      <ContentBlock sm>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h4" gutterBottom color={initialValues ? 'secondary' : 'default'}>
                {initialValues ? 'Edit' : 'Add'} list value
              </Typography>
              <Divider />
            </Grid>

            <Grid item xs={12} md={6}>
              <Field
                component={TextAutoComplete}
                label="List Name"
                name="list"
                options={listNames}
                add
                required
                validate={isRequired('List Name')}
                fieldError={validationErrors.list}
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <Field
                component={TextField}
                label="Value"
                name="value"
                required
                validate={isRequired('Value')}
                fieldError={validationErrors.value}
              />
            </Grid>

            <Grid item xs={12} md={6}>
              <Field
                component={DropDown}
                label="Status"
                name="status"
                required
                options={{ active: 'active', inactive: 'inactive' }}
                validate={isRequired('Status')}
                fieldError={validationErrors.status}
              />
            </Grid>

            <Grid item xs={12}>
              <Grid container spacing={1} justify="flex-end" alignItems="center">
                <Grid item>
                  <Button
                    variant="contained"
                    color={initialValues ? 'secondary' : 'primary'}
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {initialValues ? 'Edit' : 'Add'} <ButtonIcon component={SaveIcon} />
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </ContentBlock>
    )
  }
}

export default reduxForm({
  form: 'list',
})(ListForm)
