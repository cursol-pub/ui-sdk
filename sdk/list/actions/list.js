import { getRequest, postRequest } from './../../rest/requests'
import { defaultRequestWrapper, getParam } from './../../rest/actions/rest'

export const LIST_GET_NAMES = 'LIST_GET_NAMES'
export const LIST_GET_NAMES_SUCCESS = 'LIST_GET_NAMES_SUCCESS'
export const LIST_GET_NAMES_FAIL = 'LIST_GET_NAMES_FAIL'
export const LIST_GET_BY_NAME = 'LIST_GET_BY_NAME'
export const LIST_GET_BY_NAME_SUCCESS = 'LIST_GET_BY_NAME_SUCCESS'
export const LIST_GET_BY_NAME_FAIL = 'LIST_GET_BY_NAME_FAIL'

export const getListNames = (...actionParams) => () => dispatch => {
  const url = getParam(actionParams, 'url', 0, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 1, defaultRequestWrapper)

  dispatch({
    type: LIST_GET_NAMES,
  })

  const requestUrl = url || 'list/names'

  return requestWrapper(getRequest, requestUrl)
    .then(data => {
      dispatch({
        type: LIST_GET_NAMES_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: LIST_GET_NAMES_FAIL,
        error,
      })
    })
}

export const getListByName = (...actionParams) => name => dispatch => {
  const url = getParam(actionParams, 'url', 0, null)
  const hydrator = getParam(actionParams, 'hydrator', 1, null)
  const assoc = getParam(actionParams, 'assoc', 2, false)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 3, defaultRequestWrapper)

  dispatch({
    type: LIST_GET_BY_NAME,
    name,
  })

  const requestUrl = url || 'list/search'

  return requestWrapper(postRequest, requestUrl, { filter: { list: name } })
    .then(data => {
      dispatch({
        type: LIST_GET_BY_NAME_SUCCESS,
        name,
        data,
        assoc,
        hydrator,
      })
    })
    .catch(error => {
      dispatch({
        type: LIST_GET_BY_NAME_FAIL,
        error,
      })
    })
}

export const getAssocListByName = (...actionParams) => name => dispatch => {
  const url = getParam(actionParams, 'url', 0, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 1, defaultRequestWrapper)

  dispatch({
    type: LIST_GET_BY_NAME,
    name,
  })

  const requestUrl = url || `list/${name}`

  return requestWrapper(getRequest, requestUrl)
    .then(data => {
      dispatch({
        type: LIST_GET_BY_NAME_SUCCESS,
        name,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: LIST_GET_BY_NAME_FAIL,
        error,
      })
    })
}
