import { connect } from 'react-redux'

import RestListContainer from './../../rest/containers/RestList'
import EditForm, { PK_NAME } from './../containers/ListForm'
import FilterForm from './../containers/ListFilter'
import { restCreateNewItem, restDelete } from './../../rest/actions/rest'

const createNewAction = restCreateNewItem('listvalue')
const deleteAction = restDelete('listvalue', true, 'list')

const columns = [
  { id: 'name', label: 'List Name', numeric: false },
  { id: 'value', label: 'Value', numeric: false },
  { id: 'status', label: 'Status', numeric: false },
]

const mapStateToProps = state => {
  const {
    listvalue: { isLoading, listvalueList: list, error },
  } = state
  return {
    isLoading,
    list,
    error,
    editForm: EditForm,
    filterForm: FilterForm,
    pkName: PK_NAME,
    modelName: 'List values',
    columns,
  }
}

const mapDispatchToProps = dispatch => ({
  onCreateNew: () => dispatch(createNewAction()),
  onDelete: id => dispatch(deleteAction(id)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RestListContainer)
