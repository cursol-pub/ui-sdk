import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import RestFormContainer from './../../rest/containers/RestForm'
import ListForm from './../forms/ListForm'
import { restPost } from './../../rest/actions/rest'
import { getListNames } from './../actions/list'

const postAction = restPost('listvalue', 'list')
const getListNamesAction = getListNames()

export const PK_NAME = 'id'

class ListFormContainer extends Component {
  static propTypes = {
    onGetLists: PropTypes.func.isRequired,
    listNames: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
  }

  componentDidMount() {
    const { listNames, onGetLists, isLoading } = this.props
    if (!isLoading && (!listNames || !listNames.length)) {
      onGetLists()
    }
  }

  render() {
    const { onGetLists, ...other } = this.props
    return <RestFormContainer {...other} />
  }
}

const mapStateToProps = state => {
  const {
    listvalue: { isSubmitting, isLoadingItem, list, error },
    list: { isLoading, listNames },
  } = state

  return {
    isSubmitting,
    isLoadingItem,
    isLoading,
    initialValues: list,
    error,
    formComponent: ListForm,
    pkName: PK_NAME,
    listNames,
  }
}

const mapDispatchToProps = dispatch => ({
  onPost: values => dispatch(postAction(values)),
  onGetLists: () => dispatch(getListNamesAction()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListFormContainer)
