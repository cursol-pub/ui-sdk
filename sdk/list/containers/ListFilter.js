import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import RestFormContainer from './../../rest/containers/RestForm'
import ListFilter from './../forms/ListFilter'
import { restGetFilteredList } from './../../rest/actions/rest'
import { getListNames } from './../actions/list'
import Auth from './../../auth/containers/Auth'

const postAction = restGetFilteredList('listvalue', 'list/search')
const getListNamesAction = getListNames()

class ListFilterContainer extends Component {
  static propTypes = {
    onPost: PropTypes.func.isRequired,
    onGetListNames: PropTypes.func.isRequired,
    listNames: PropTypes.array,
    isLoading: PropTypes.bool,
  }

  componentDidMount() {
    const { listNames, onGetListNames, isLoading } = this.props
    if (!isLoading && (!listNames || !listNames.length)) {
      onGetListNames()
    }
  }

  render() {
    const { onGetListNames, ...other } = this.props

    return (
      <Auth user redirect>
        <RestFormContainer {...other} />
      </Auth>
    )
  }
}

const mapStateToProps = state => {
  const {
    listvalue: { filter: initialValues, error, isSubmitting },
    list: { isLoading, listNames },
  } = state

  return {
    isSubmitting,
    isLoading,
    listNames,
    formComponent: ListFilter,
    initialValues,
    error,
  }
}

const mapDispatchToProps = dispatch => ({
  onPost: values => dispatch(postAction(values)),
  onGetListNames: () => dispatch(getListNamesAction()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListFilterContainer)
