import 'core-js/stable'

import Fingerprint from 'fingerprintjs2'

let deviceFingerprint = null

export const hasDeviceFingerprint = () => {
  return !!(deviceFingerprint && deviceFingerprint !== '')
}

export const getDeviceFingerprint = () => {
  return deviceFingerprint
}

export const getDeviceFingerprintPromise = () => {
  if (!hasDeviceFingerprint()) {
    return Fingerprint.getPromise({
      fonts: { extendedJsFonts: false },
      excludes: { enumerateDevices: true },
    }).then(components => {
      const values = components.map(comp => {
        const val = Array.isArray(comp.value) ? comp.value.join(',') : comp.value
        return `${comp.key}:${val}`
      })
      deviceFingerprint = Fingerprint.x64hash128(values.join('|'), 31)

      return deviceFingerprint
    })
  }

  return Promise.resolve(deviceFingerprint)
}
