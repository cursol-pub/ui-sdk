export const addAsyncScript = (src, id) => {
  const n = document.createElement('script')
  n.async = !0
  n.defer = !0
  n.id = id
  n.src = src
  document.getElementsByTagName('head')[0].appendChild(n)
}

export const removeScript = id => {
  const s = document.getElementById(id)
  if (s && s.parentNode) {
    s.parentNode.removeChild(s)
  }
}
