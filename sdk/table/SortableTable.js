import { Component } from 'react'
import PropTypes from 'prop-types'
import keycode from 'keycode'
import classNames from 'classnames'

import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Checkbox from '@material-ui/core/Checkbox'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import IconButton from './../layout/IconButton'
import Grid from '@material-ui/core/Grid'

import SortableTableHead from './SortableTableHead'
import SortableTableToolbar from './SortableTableToolbar'
import InfiniteLoader from './../layout/InfiniteLoader'

export const ORDER_ASC = 'asc'
export const ORDER_DESC = 'desc'
export const DEFAULT_ORDER = ORDER_ASC
const INDEX_NAME = 'rowIndex'
const PER_PAGE_DEFAULT = 20

const styleSheet = theme => ({
  root: {
    width: '100%',
    height: '100%',
  },
  flexible: {
    display: 'flex',
    flex: '1 1 auto',
  },
  numeric: {
    width: 100,
  },
  narrow: {
    width: 140,
  },
  medium: {
    width: 200,
  },
  cell: {
    padding: theme.spacing(1),
    whiteSpace: 'normal',
  },
  row: {
    height: 'auto',
  },
  actions: {
    whiteSpace: 'nowrap',
    width: 84,
  },
  checkbox: {
    width: 28,
    height: 28,
  },
  actionicon: {
    width: 24,
    height: 24,
    opacity: 0,
    transition: 'opacity .4s ease',
  },
})

class SortableTable extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    selectSingle: PropTypes.bool.isRequired,
    onClick: PropTypes.func,
    onSelect: PropTypes.func,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    actions: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    order: PropTypes.string,
    orderBy: PropTypes.string,
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    pkName: PropTypes.string.isRequired,
    perPage: PropTypes.number.isRequired,
  }

  static defaultProps = {
    order: DEFAULT_ORDER,
    orderBy: INDEX_NAME,
    pkName: INDEX_NAME,
    selectSingle: false,
    perPage: PER_PAGE_DEFAULT,
  }

  state = {
    selected: [],
    shownRowsCount: PER_PAGE_DEFAULT,
    search: '',
    order: DEFAULT_ORDER,
    orderBy: INDEX_NAME,
    data: [],
  }

  isLoading = false

  componentDidMount() {
    const { order, orderBy, data } = this.props
    this.sortData(order, orderBy, this.prepareData(data))
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      const { order, orderBy } = this.state
      const newData = this.prepareData(this.props.data)
      this.updateSelected(newData)
      this.sortData(order, orderBy, newData)
    }
    this.isLoading = false
  }

  showMore = () => {
    if (this.isLoading) {
      return
    }

    this.isLoading = true
    const { perPage } = this.props
    let { shownRowsCount, data } = this.state
    shownRowsCount += perPage

    if (shownRowsCount > data.length) {
      shownRowsCount = data.length
    }

    this.setState({ shownRowsCount })
  }

  prepareData(data) {
    let numOfItems = 1
    return data.map(item => {
      return { rowIndex: numOfItems++, ...item }
    })
  }

  sortData(order, orderBy, newData) {
    const data = newData.sort((a, b) => {
      let prevItem = a[orderBy] && typeof a[orderBy] !== 'undefined' ? this.showValue(a[orderBy]) : ''
      if (typeof prevItem === 'string') {
        prevItem = prevItem.toLowerCase()
      }

      let nextItem = b[orderBy] && typeof b[orderBy] !== 'undefined' ? this.showValue(b[orderBy]) : ''
      if (typeof nextItem === 'string') {
        nextItem = nextItem.toLowerCase()
      }

      if (nextItem > prevItem) {
        return order === ORDER_ASC ? 1 : -1
      }
      if (nextItem < prevItem) {
        return order === ORDER_ASC ? -1 : 1
      }
      return 0
    })
    this.setState({ data, order, orderBy })
  }

  updateSelected(newData) {
    const { pkName } = this.props
    const { selected } = this.state

    if (!newData.length || !pkName || typeof newData[0][pkName] === 'undefined') {
      this.handleSelect([])
      return
    }

    const selectedByPk = this.getPksByIndexes(selected)
    const newSelected = newData.reduce((selected, item) => {
      if (selectedByPk.indexOf(item[pkName]) > -1) {
        selected.push(item[INDEX_NAME])
      }
      return selected
    }, [])

    this.handleSelect(newSelected)
  }

  handleSelect(selected) {
    const { onSelect } = this.props

    this.setState({ selected })
    if (onSelect) {
      onSelect(this.getPksByIndexes(selected))
    }
  }

  getPksByIndexes(indexes) {
    let { pkName } = this.props

    return indexes.reduce((selected, itemIndex) => {
      const itemData = this.getItemByIndex(itemIndex)
      selected.push(itemData[pkName])
      return selected
    }, [])
  }

  getItemByIndex(itemIndex) {
    const { data } = this.state

    return data.reduce((acc, item) => {
      return item[INDEX_NAME] === itemIndex ? item : acc
    }, {})
  }

  handleRequestSort = (event, property) => {
    let order = DEFAULT_ORDER

    if (this.state.orderBy === property && this.state.order === DEFAULT_ORDER) {
      order = DEFAULT_ORDER === ORDER_ASC ? ORDER_DESC : ORDER_ASC
    }

    this.sortData(order, property, this.state.data)
  }

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.handleSelect(this.state.data.map(n => n[INDEX_NAME]))
      return
    }
    this.handleSelect([])
  }

  handleKeyDown = (event, rowIndex) => {
    if (keycode(event) === 'space') {
      this.handleClick(event, rowIndex)
    }
  }

  handleClick = (event, rowIndex) => {
    if (
      ['svg', 'path'].indexOf(event.target.tagName.toLowerCase()) > -1 ||
      event.target.className.indexOf('MuiIconButton') > -1
    ) {
      return
    }
    const { selectSingle, onClick } = this.props

    if (onClick) {
      onClick(this.getItemByIndex(rowIndex))
    }

    const { selected } = this.state
    const selectedIndex = selected.indexOf(rowIndex)
    let newSelected = []

    if (selectSingle) {
      newSelected = [rowIndex]
    } else if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, rowIndex)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1))
    }

    this.handleSelect(newSelected)
  }

  isSelected = rowIndex => this.state.selected.indexOf(rowIndex) > -1

  onEdit = () => {
    const { selected } = this.state
    const { onEdit } = this.props

    if (onEdit) {
      onEdit(this.getPksByIndexes(selected))
    }
  }

  onDelete = () => {
    const { selected } = this.state
    const { onDelete } = this.props

    if (onDelete) {
      onDelete(this.getPksByIndexes(selected))
    }
  }

  onSearch = event => {
    const { order, orderBy } = this.state
    const { data } = this.props
    const search = event.target.value

    const filtered = data.filter(
      item =>
        JSON.stringify(item)
          .toLowerCase()
          .indexOf(search.toLowerCase()) > -1
    )
    const newData = this.prepareData(filtered)
    this.updateSelected(newData)
    this.sortData(order, orderBy, newData)

    this.setState({ search })
  }

  showValue = item => {
    if (Array.isArray(item)) {
      return item.slice(0, 3).join(', ')
    }

    if (typeof item === 'object' && item) {
      if (item.hasOwnProperty('props') || item.hasOwnProperty('propTypes')) {
        return item
      }

      if (item.hasOwnProperty('value')) {
        return this.showValue(item.value)
      }

      if (item instanceof Date) {
        return item.toLocaleDateString('en-GB', {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
        })
      }

      return JSON.stringify(item)
    }

    if (typeof item === 'boolean') {
      return item ? 'Yes' : 'No'
    }

    if (typeof item === 'string') {
      return item.replace(/\n/g, '<br />')
    }

    return item
  }

  render() {
    const { classes, columns, name, pkName, onEdit, onDelete, selectSingle, actions } = this.props
    const { data, order, orderBy, search, selected, shownRowsCount } = this.state

    return (
      <Grid container spacing={1} direction={'column'} alignItems={'stretch'} className={classes.flexible}>
        <SortableTableToolbar
          numSelected={selected.length}
          name={name}
          onEdit={onEdit ? this.onEdit : null}
          onDelete={onDelete ? this.onDelete : null}
          searchValue={search}
          onSearch={this.onSearch}
          selectSingle={selectSingle}
          actions={actions}
        />

        <Grid item className={classes.flexible}>
          <InfiniteLoader
            scrollHandler={this.showMore}
            isEnabled={!!(shownRowsCount && data && shownRowsCount < data.length)}
            bottomOffset={800}
            isLoading={false}
          >
            <Table>
              <SortableTableHead
                order={order}
                orderBy={orderBy}
                columns={columns}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                selectSingle={selectSingle}
              />
              <TableBody>
                {data &&
                  data.slice(0, shownRowsCount).map(item => {
                    const isSelected = this.isSelected(item[INDEX_NAME])
                    return (
                      <TableRow
                        hover
                        onClick={event => this.handleClick(event, item[INDEX_NAME])}
                        onKeyDown={event => this.handleKeyDown(event, item[INDEX_NAME])}
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex="-1"
                        key={item[INDEX_NAME]}
                        selected={isSelected}
                        className={classNames('row-hover', classes.row)}
                      >
                        {!selectSingle ? (
                          <TableCell
                            className={classNames(classes.cell, classes.actions)}
                            key={`${item[INDEX_NAME]}-select`}
                          >
                            <Checkbox checked={isSelected} className={classes.checkbox} />

                            {onEdit ? (
                              <IconButton
                                inline
                                hoverable
                                icon={EditIcon}
                                onClick={() => {
                                  onEdit([item[pkName]])
                                }}
                              />
                            ) : null}

                            {onDelete ? (
                              <IconButton
                                inline
                                hoverable
                                icon={DeleteIcon}
                                onClick={() => {
                                  onDelete([item[pkName]])
                                }}
                              />
                            ) : null}
                          </TableCell>
                        ) : null}

                        {columns.map(column => {
                          return (
                            <TableCell
                              className={classNames(classes.cell, {
                                [classes.numeric]: column.numeric,
                                [classes.narrow]: column.narrow,
                                [classes.medium]: column.medium,
                              })}
                              key={`${item[INDEX_NAME]}-${column.id}`}
                            >
                              {column.component ? (
                                item[column.id]
                              ) : (
                                <span dangerouslySetInnerHTML={{ __html: this.showValue(item[column.id]) }} />
                              )}
                            </TableCell>
                          )
                        })}
                      </TableRow>
                    )
                  })}
              </TableBody>
            </Table>
          </InfiniteLoader>
        </Grid>
      </Grid>
    )
  }
}

export default withStyles(styleSheet)(SortableTable)
