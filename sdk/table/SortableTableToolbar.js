import classNames from 'classnames'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import Grid from '@material-ui/core/Grid'
import IconButton from './../layout/IconButton'
import Divider from 'cursol/sdk/layout/Divider'
import TextField from '@material-ui/core/TextField'

const toolbarStyleSheet = theme => ({
  toolbar: {
    padding: theme.spacing(0, 1),
  },
  title: {
    height: 56,
    paddingTop: `${theme.spacing(1.5)}px !important`,
  },
  iconCont: {
    paddingTop: `${theme.spacing(1.5)}px !important`,
  },
})

const SortableTableToolbar = props => {
  const { numSelected, classes, name, onDelete, onEdit, onSearch, searchValue, selectSingle, actions: Actions } = props
  const selectable = !selectSingle && numSelected > 0

  return (
    <Grid item>
      <Toolbar className={classNames(classes.toolbar)}>
        <Grid container spacing={1} justify="space-between">
          <Grid item className={classes.title}>
            <Typography variant="h4" color={'primary'}>
              {name}
            </Typography>
          </Grid>

          <Grid item>
            <Grid container spacing={1} justify="flex-end">
              {Actions ? (
                <Grid item className={classes.iconCont}>
                  <Actions />
                </Grid>
              ) : null}

              {selectable && numSelected === 1 && onEdit ? (
                <Grid item className={classes.iconCont}>
                  <IconButton
                    onClick={() => {
                      onEdit()
                    }}
                    icon={EditIcon}
                  />
                </Grid>
              ) : null}

              <Grid item className={classes.iconCont}>
                {selectable && onDelete ? (
                  <IconButton
                    onClick={() => {
                      onDelete()
                    }}
                    icon={DeleteIcon}
                  />
                ) : null}
              </Grid>

              <Grid item>
                <TextField label="Search" margin="dense" variant="outlined" onChange={onSearch} value={searchValue} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>

      <Divider color={'default'} />
    </Grid>
  )
}

SortableTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  numSelected: PropTypes.number.isRequired,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onSearch: PropTypes.func,
  searchValue: PropTypes.string,
  selectSingle: PropTypes.bool.isRequired,
  actions: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
}

SortableTableToolbar.defaultProps = {
  selectSingle: false,
}

export default withStyles(toolbarStyleSheet)(SortableTableToolbar)
