import { Component } from 'react'
import PropTypes from 'prop-types'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Checkbox from '@material-ui/core/Checkbox'
import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  cell: {
    padding: theme.spacing(1),
    whiteSpace: 'nowrap',
    color: theme.palette.primary.main,
  },
  checkbox: {
    width: 28,
    height: 28,
  },
})

class SortableTableHead extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    columns: PropTypes.array.isRequired,
    selectSingle: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    selectSingle: false,
  }

  createSortHandler = property => event => {
    const { onRequestSort } = this.props
    onRequestSort(event, property)
  }

  render() {
    const { classes, onSelectAllClick, order, orderBy, columns, selectSingle } = this.props

    return (
      <TableHead>
        <TableRow>
          {!selectSingle ? (
            <TableCell className={classes.cell}>
              <Checkbox onChange={onSelectAllClick} className={classes.checkbox} />
            </TableCell>
          ) : null}

          {columns.map(column => {
            return (
              <TableCell key={column.id} className={classes.cell} variant={'head'}>
                <TableSortLabel
                  active={orderBy === column.id}
                  direction={order}
                  onClick={this.createSortHandler(column.id)}
                >
                  {column.label}
                </TableSortLabel>
              </TableCell>
            )
          }, this)}
        </TableRow>
      </TableHead>
    )
  }
}

export default withStyles(styleSheet)(SortableTableHead)
