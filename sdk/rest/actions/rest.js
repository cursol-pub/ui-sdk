import { getRequest, postRequest, deleteRequest, putRequest } from './../requests'

export const SUFFIX_NEW_ITEM = '_NEW_ITEM'
export const SUFFIX_GET_LIST = '_GET_LIST'
export const SUFFIX_GET_LIST_SUCCESS = '_GET_LIST_SUCCESS'
export const SUFFIX_GET_LIST_FAIL = '_GET_LIST_FAIL'
export const SUFFIX_GET_BY_ID = '_GET_BY_ID'
export const SUFFIX_GET_BY_ID_SUCCESS = '_GET_BY_ID_SUCCESS'
export const SUFFIX_GET_BY_ID_FAIL = '_GET_BY_ID_FAIL'
export const SUFFIX_POST = '_POST'
export const SUFFIX_POST_SUCCESS = '_POST_SUCCESS'
export const SUFFIX_POST_FAIL = '_POST_FAIL'
export const SUFFIX_PUT = '_PUT'
export const SUFFIX_PUT_SUCCESS = '_PUT_SUCCESS'
export const SUFFIX_PUT_FAIL = '_PUT_FAIL'
export const SUFFIX_DELETE = '_DELETE'
export const SUFFIX_DELETE_SUCCESS = '_DELETE_SUCCESS'
export const SUFFIX_DELETE_FAIL = '_DELETE_FAIL'

export const defaultRequestWrapper = (handler, ...params) => {
  return handler(...params)
}

export const getParam = (params, name, pos, defValue) => {
  const assocParams = params[0] && typeof params[0] === 'object' ? params[0] : {}
  return assocParams.hasOwnProperty(name) ? assocParams[name] : params[pos] || defValue
}

export const restCreateNewItem = modelName => () => dispatch => {
  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_NEW_ITEM}`,
  })
}

export const restGetList = (...actionParams) => (queryString = null) => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const url = getParam(actionParams, 'url', 1, null)
  const hydrator = getParam(actionParams, 'hydrator', 2, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 3, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST}`,
  })

  let requestUrl = url || modelName.toLowerCase()
  if (queryString) {
    requestUrl += queryString
  }

  return requestWrapper(getRequest, requestUrl)
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST_SUCCESS}`,
        data,
        hydrator,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST_FAIL}`,
        error,
      })
    })
}

export const restGetFilteredList = (...actionParams) => (filter, pagination = null) => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const url = getParam(actionParams, 'url', 1, null)
  const hydrator = getParam(actionParams, 'hydrator', 2, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 3, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST}`,
    filter,
    pagination,
  })

  const requestUrl = url || modelName.toLowerCase()

  return requestWrapper(postRequest, requestUrl, { filter, pagination })
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST_SUCCESS}`,
        data,
        hydrator,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_LIST_FAIL}`,
        error,
      })
    })
}

export const restGetById = (...actionParams) => id => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const restful = getParam(actionParams, 'restful', 1, false)
  const hydrator = getParam(actionParams, 'hydrator', 2, null)
  const url = getParam(actionParams, 'url', 3, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 4, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID}`,
  })

  const modelUrl = url || modelName.toLowerCase()
  const requestUrl = restful ? `${modelUrl}/${id}` : `${modelUrl}?id=${id}`

  return requestWrapper(getRequest, requestUrl)
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID_SUCCESS}`,
        data,
        hydrator,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID_FAIL}`,
        error,
      })
    })
}

export const restPost = (...actionParams) => formData => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const url = getParam(actionParams, 'url', 1, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 2, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_POST}`,
    data: formData,
  })

  const requestUrl = url || modelName.toLowerCase()

  return requestWrapper(postRequest, requestUrl, formData)
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_POST_SUCCESS}`,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_POST_FAIL}`,
        error,
      })
    })
}

export const restPut = (...actionParams) => (id, formData) => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const restful = getParam(actionParams, 'restful', 1, false)
  const url = getParam(actionParams, 'url', 2, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 3, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_PUT}`,
    data: {
      id,
      formData,
    },
  })

  const modelUrl = url || modelName.toLowerCase()
  const routeUrl = restful ? `${modelUrl}/${id}` : `${modelUrl}?id=${id}`

  return requestWrapper(putRequest, routeUrl, formData)
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_PUT_SUCCESS}`,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_PUT_FAIL}`,
        error,
      })
    })
}

export const restDelete = (...actionParams) => id => dispatch => {
  const modelName = getParam(actionParams, 'modelName', 0, '')
  const restful = getParam(actionParams, 'restful', 1, false)
  const url = getParam(actionParams, 'url', 2, null)
  const requestWrapper = getParam(actionParams, 'requestWrapper', 3, defaultRequestWrapper)

  dispatch({
    type: `${modelName.toUpperCase()}${SUFFIX_DELETE}`,
    data: id,
  })

  const modelUrl = url || modelName.toLowerCase()
  const requestUrl = restful ? `${modelUrl}/${id}` : `${modelUrl}?id=${id}`

  return requestWrapper(deleteRequest, requestUrl)
    .then(data => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_DELETE_SUCCESS}`,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: `${modelName.toUpperCase()}${SUFFIX_DELETE_FAIL}`,
        error,
      })
    })
}
