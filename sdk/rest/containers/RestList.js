import { Component } from 'react'
import PropTypes from 'prop-types'

import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import Dialog from '@material-ui/core/Dialog'
import Divider from './../../layout/Divider'
import Fade from '@material-ui/core/Fade'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'

import Auth from './../../auth/containers/Auth'
import ContentBlock from './../../layout/ContentBlock'
import ButtonIcon from './../../icon/ButtonIcon'
import SortableTable, { ORDER_ASC } from './../../table/SortableTable'

const styleSheet = theme => ({
  dialog: {
    maxHeight: '100vh',
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  dialogPaper: {
    overflowY: 'visible',
  },
  flexible: {
    display: 'flex',
    flex: '1 1 auto',
  },
  emptyResult: {
    margin: '0 auto',
    padding: '100px !important',
  },
})

class RestListContainer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onCreateNew: PropTypes.func,
    onGetList: PropTypes.func,
    onGetById: PropTypes.func,
    onDelete: PropTypes.func,
    isSubmitting: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    columns: PropTypes.array.isRequired,
    list: PropTypes.array,
    error: PropTypes.object,
    pkName: PropTypes.string.isRequired,
    modelName: PropTypes.string.isRequired,
    contentWidth: PropTypes.string.isRequired,
    dialogWidth: PropTypes.string.isRequired,
    editForm: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    filterForm: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    editFormProps: PropTypes.object,
    editOnCreate: PropTypes.bool,
  }

  static defaultProps = {
    pkName: 'id',
    contentWidth: 'lg',
    dialogWidth: 'sm',
    editOnCreate: false,
    isSubmitting: false,
    isLoading: true,
  }

  state = {
    formOpen: false,
  }

  componentDidMount() {
    const { list } = this.props
    if (!list || !list.length) {
      this.getList()
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.isSubmitting === true &&
      this.props.isSubmitting === false &&
      !this.props.error &&
      !this.props.editOnCreate
    ) {
      this.handleFormClose()
    }
  }

  getList = () => {
    const { onGetList, filterForm } = this.props
    if (onGetList && !filterForm) {
      onGetList()
    }
  }

  onEdit = selected => {
    if (selected.length !== 1) {
      return
    }

    const { onGetById } = this.props
    selected.map(pk => {
      if (onGetById) {
        onGetById(pk)
        this.handleFormOpen()
      }
    })
  }

  onDelete = selected => {
    const { onDelete } = this.props
    selected.map(pk => {
      if (onDelete && confirm('Are you sure you want to delete item?')) {
        onDelete(pk)
      }
    })
  }

  handleNewItem = () => {
    const { onCreateNew } = this.props
    if (onCreateNew) {
      onCreateNew()
      this.handleFormOpen()
    }
  }

  handleFormOpen = () => {
    this.setState({ formOpen: true })
  }

  handleFormClose = () => {
    this.setState({ formOpen: false })
  }

  render() {
    const {
      classes,
      pkName,
      modelName,
      columns,
      list,
      contentWidth,
      dialogWidth,
      isLoading,
      isSubmitting,
      editForm: EditForm,
      editFormProps,
      filterForm: FilterForm,
      onGetById,
      onCreateNew,
      onDelete,
    } = this.props
    const contentBlockProps = { [contentWidth]: true }

    return (
      <Auth user redirect>
        <ContentBlock {...contentBlockProps} stretch>
          <Grid container spacing={1} direction={'column'} alignItems={'stretch'} className={classes.flexible}>
            {isLoading || isSubmitting ? (
              <Grid item>
                <LinearProgress />
              </Grid>
            ) : null}

            {FilterForm ? (
              <Grid item>
                <FilterForm />
              </Grid>
            ) : null}

            {FilterForm ? (
              <Grid item>
                <Divider />
              </Grid>
            ) : null}

            {list && list.length ? (
              <Grid item className={classes.flexible}>
                <SortableTable
                  name={modelName}
                  orderBy={pkName}
                  order={ORDER_ASC}
                  columns={columns}
                  data={list}
                  pkName={pkName}
                  onEdit={onGetById ? this.onEdit : null}
                  onDelete={onDelete ? this.onDelete : null}
                />
              </Grid>
            ) : null}

            {!list && !isLoading ? (
              <Grid item className={classes.emptyResult}>
                <Typography variant="h3">Requested list has empty result set</Typography>
              </Grid>
            ) : null}

            {onCreateNew ? (
              <Grid item>
                <Grid container spacing={1} justify="flex-end" alignItems="center">
                  <Grid item>
                    <Button variant="contained" color="primary" onClick={this.handleNewItem}>
                      Add {modelName} <ButtonIcon component={AddIcon} />
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            ) : null}
          </Grid>

          {EditForm ? (
            <Dialog
              maxWidth={dialogWidth}
              TransitionComponent={Fade}
              open={this.state.formOpen}
              onClose={this.handleFormClose}
              className={classes.dialog}
              PaperProps={{
                classes: {
                  root: classes.dialogPaper,
                },
              }}
            >
              <EditForm {...editFormProps} />
            </Dialog>
          ) : null}
        </ContentBlock>
      </Auth>
    )
  }
}

export default withStyles(styleSheet)(RestListContainer)
