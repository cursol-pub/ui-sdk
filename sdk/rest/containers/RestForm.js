import { Component } from 'react'
import PropTypes from 'prop-types'

import { parseValidationErrors } from './../../form/Field/validation'

export default class RestFormContainer extends Component {
  static propTypes = {
    onPost: PropTypes.func.isRequired,
    onPut: PropTypes.func,
    isSubmitting: PropTypes.bool.isRequired,
    initialValues: PropTypes.object,
    error: PropTypes.object,
    syncErrors: PropTypes.object,
    formComponent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
    pkName: PropTypes.string.isRequired,
    filterValues: PropTypes.func,
  }

  static defaultProps = {
    pkName: 'id',
  }

  onSubmit = values => {
    this.submittedValues = values
    const { filterValues, initialValues, onPost, onPut, pkName } = this.props

    const filteredValues = filterValues ? filterValues(values) : values

    if (initialValues && onPut) {
      onPut(values[pkName], filteredValues)
    } else {
      onPost(filteredValues)
    }
  }

  onSubmitFail = errors => {
    if (!errors || !Object.keys(errors).length) {
      return
    }

    setTimeout(this.scrollToError, 100)
  }

  scrollToError = () => {
    const els = document.querySelectorAll('.field.error')
    if (!els || els.length < 1) {
      return
    }

    const wscroll = window.pageYOffset || document.documentElement.scrollTop
    window.scroll(0, els[0].getBoundingClientRect().top + wscroll - window.innerHeight / 3)
  }

  render() {
    const { onPost, onPut, error, formComponent: FormComponent, ...other } = this.props

    return (
      <FormComponent
        onSubmit={this.onSubmit}
        onSubmitFail={this.onSubmitFail}
        validationErrors={parseValidationErrors(error, this.submittedValues)}
        {...other}
      />
    )
  }
}
