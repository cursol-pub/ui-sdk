import Raven from 'raven-js'
import { ERROR_VALIDATION_CODE } from './../form/Field/validation'

export const LogError = error => {
  if (!error) {
    return
  }

  if (error.code && error.message) {
    if (error.code === ERROR_VALIDATION_CODE) {
      return
    }

    Raven.captureException(`${error.code}: ${JSON.stringify(error.message)}`)
    return
  }

  Raven.captureException(JSON.stringify(error))
}
