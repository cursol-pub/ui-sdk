import {
  SUFFIX_NEW_ITEM,
  SUFFIX_GET_LIST,
  SUFFIX_GET_LIST_SUCCESS,
  SUFFIX_GET_LIST_FAIL,
  SUFFIX_GET_BY_ID,
  SUFFIX_GET_BY_ID_SUCCESS,
  SUFFIX_GET_BY_ID_FAIL,
  SUFFIX_POST,
  SUFFIX_POST_SUCCESS,
  SUFFIX_POST_FAIL,
  SUFFIX_PUT,
  SUFFIX_PUT_SUCCESS,
  SUFFIX_PUT_FAIL,
  SUFFIX_DELETE,
  SUFFIX_DELETE_SUCCESS,
  SUFFIX_DELETE_FAIL,
} from './../actions/rest'

const restReducers = (modelName, pkName) => {
  const modelListName = `${modelName.toLowerCase()}List`
  const modelEntityName = modelName.toLowerCase()
  const initialState = {
    isLoading: false,
    isLoadingItem: false,
    isSubmitting: false,
    [modelListName]: [],
    [modelEntityName]: null,
    error: null,
    filter: null,
    pagination: null,
  }

  const reducers = (state = initialState, actionData = null) => {
    switch (actionData.type) {
      case `${modelName.toUpperCase()}${SUFFIX_NEW_ITEM}`:
        return {
          ...state,
          isLoading: false,
          [modelEntityName]: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_LIST}`:
        return {
          ...state,
          isLoading: true,
          filter: actionData.filter ? actionData.filter : null,
          pagination: actionData.pagination ? actionData.pagination : null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_LIST_SUCCESS}`:
        let listData = actionData.data.list || []

        if (typeof actionData.hydrator === 'function') {
          listData = listData.map(actionData.hydrator)
        }

        if (state.pagination) {
          listData = [].concat(state[modelListName].slice(0), listData)
        }
        return {
          ...state,
          isLoading: false,
          [modelListName]: listData,
          pagination: actionData.data.pagination ? actionData.data.pagination : null,
          error: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_LIST_FAIL}`:
        return {
          ...state,
          isLoading: false,
          error: actionData.error,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID}`:
        return {
          ...state,
          isLoadingItem: true,
          [modelEntityName]: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID_SUCCESS}`:
        let item = actionData.data.item || null

        if (typeof actionData.hydrator === 'function') {
          item = actionData.hydrator(item)
        }

        return {
          ...state,
          [modelEntityName]: item,
          isLoadingItem: false,
          error: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_GET_BY_ID_FAIL}`:
        return {
          ...state,
          isLoadingItem: false,
          [modelEntityName]: null,
          error: actionData.error,
        }

      case `${modelName.toUpperCase()}${SUFFIX_POST}`:
        return {
          ...state,
          [modelEntityName]: null,
          isSubmitting: true,
        }

      case `${modelName.toUpperCase()}${SUFFIX_POST_SUCCESS}`:
        const listAfterPost = state[modelListName].slice(0)
        if (actionData.data.item) {
          listAfterPost.push(actionData.data.item)
        }
        return {
          ...state,
          [modelEntityName]: actionData.data.item || null,
          [modelListName]: listAfterPost,
          isSubmitting: false,
          error: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_POST_FAIL}`:
        return {
          ...state,
          isSubmitting: false,
          error: actionData.error,
        }

      case `${modelName.toUpperCase()}${SUFFIX_PUT}`:
        return {
          ...state,
          isSubmitting: true,
        }

      case `${modelName.toUpperCase()}${SUFFIX_PUT_SUCCESS}`:
        let listAfterPut = state[modelListName].slice(0)
        if (actionData.data.item) {
          listAfterPut = listAfterPut.map(item =>
            item[pkName] === actionData.data.item[pkName] ? actionData.data.item : item
          )
        }
        return {
          ...state,
          [modelEntityName]: actionData.data.item || null,
          [modelListName]: listAfterPut,
          isSubmitting: false,
          error: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_PUT_FAIL}`:
        return {
          ...state,
          isSubmitting: false,
          error: actionData.error,
        }

      case `${modelName.toUpperCase()}${SUFFIX_DELETE}`:
        return {
          ...state,
          isSubmitting: true,
        }

      case `${modelName.toUpperCase()}${SUFFIX_DELETE_SUCCESS}`:
        const deletedPk = actionData.data.id || actionData.data[pkName]
        const updatedList = state[modelListName].reduce((list, item) => {
          if (item[pkName] !== deletedPk) {
            list.push(item)
          }
          return list
        }, [])

        return {
          ...state,
          isSubmitting: false,
          [modelListName]: updatedList,
          error: null,
        }

      case `${modelName.toUpperCase()}${SUFFIX_DELETE_FAIL}`:
        return {
          ...state,
          isSubmitting: false,
          error: actionData.error,
        }

      default:
        return state
    }
  }

  return reducers
}

export default restReducers
