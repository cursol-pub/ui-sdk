import AppConfig from 'AppConfig'
import axios from 'axios'
import { getToken, hasToken, setToken } from './../auth/jwt'
import { LogError } from '../rest/index'

let getTokenPromise = null

const headers = () => {
  return { headers: hasToken() ? { Authorization: getToken() } : {} }
}

const handleSuccess = response => {
  if (!response || !response.data || response.code > 300) {
    throw new Error('Wrong HTTP response')
  }
  if (!response.data.data) {
    throw new Error('Wrong API response')
  }

  return response.data.data
}

const handleFail = res => {
  let error = { message: 'API error' }

  if (res instanceof Error) {
    error =
      res.response && res.response.data && res.response.data.error ? res.response.data.error : { message: res.message }
  }

  if (res && res.error) {
    error = res.error
  }

  if (error && (error.code === 'ERROR_AUTHORIZATION' || error.code === 'ERROR_JWT')) {
    setToken(null)
  }

  LogError(error)

  throw error
}

export const getRequest = (url, additionalHeaders = {}) => {
  const headersObj = headers()
  headersObj.headers = Object.assign(headersObj.headers ? headersObj.headers : {}, additionalHeaders)

  return axios
    .get(`${AppConfig.ServerApi}/${url}`, headersObj)
    .then(handleSuccess)
    .catch(handleFail)
}

export const postRequest = (url, data) => {
  return axios
    .post(`${AppConfig.ServerApi}/${url}`, data, headers())
    .then(handleSuccess)
    .catch(handleFail)
}

export const deleteRequest = url => {
  return axios
    .delete(`${AppConfig.ServerApi}/${url}`, headers())
    .then(handleSuccess)
    .catch(handleFail)
}

export const putRequest = (url, data) => {
  return axios
    .put(`${AppConfig.ServerApi}/${url}`, data, headers())
    .then(handleSuccess)
    .catch(handleFail)
}

export const tokenRequiredRequest = getTokenUrl => (handler, ...params) => {
  if (hasToken()) {
    return handler(...params)
  }

  if (!getTokenPromise) {
    getTokenPromise = getRequest(getTokenUrl).then(data => {
      getTokenPromise = null
      if (!data.token) {
        throw new Error('token required')
      }

      setToken(data.token)
    })
  }

  return getTokenPromise.then(() => {
    return handler(...params)
  })
}
