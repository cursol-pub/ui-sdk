import { Component } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'

class BundleComponent extends Component {
  static propTypes = {
    loader: PropTypes.func.isRequired,
    children: PropTypes.func.isRequired,
  }

  state = {
    mod: null,
  }

  componentDidMount() {
    this.load(this.props)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.loader !== this.props.loader) {
      this.load(this.props)
    }
  }

  load = ({ loader }) => {
    this.setState({
      mod: null,
    })
    loader(mod => {
      this.setState({
        mod: mod.default ? mod.default : mod,
      })
    })
  }

  render() {
    return this.state.mod ? this.props.children(this.state.mod) : <CircularProgress />
  }
}

// eslint-disable-next-line react/display-name
export default BundleLoader => props => (
  <BundleComponent loader={BundleLoader}>{LoadedComponent => <LoadedComponent {...props} />}</BundleComponent>
)
