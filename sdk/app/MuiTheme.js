import PropTypes from 'prop-types'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import { blue, red } from '@material-ui/core/colors'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: {
      main: '#00ACC0',
      contrastText: '#fff',
    },
    error: red,
  },
  typography: {
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 300,
    fontWeightMedium: 400,
  },
})

const MuiTheme = ({ children }) => <ThemeProvider theme={theme}>{children}</ThemeProvider>

MuiTheme.propTypes = {
  children: PropTypes.node.isRequired,
}

export default MuiTheme
