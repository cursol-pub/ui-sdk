import PropTypes from 'prop-types'
import bundle from './../bundle'

import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'

import AppBarContentLoader from 'bundle-loader?lazy&name=[name]!./../appbar/components/AppBarContent'
const AppBarContent = bundle(AppBarContentLoader)

const styleSheet = theme => ({
  root: {
    display: 'flex',
    minHeight: '100vh',
    alignItems: 'stretch',
  },
  content: {
    flex: '1 1 100%',
    alignItems: 'flex-start',
    display: 'flex',
    margin: '0 auto',
    maxWidth: '100%',
    padding: theme.spacing(4),
    paddingTop: 56 + theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(6),
      paddingTop: theme.spacing(16),
    },
  },
  appBar: {
    minHeight: 56,
    [theme.breakpoints.up('sm')]: {
      minHeight: 64,
    },
  },
})

const Layout = ({ children, classes, footer: Footer, ...other }) => {
  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar}>
        <AppBarContent {...other} />
      </AppBar>

      <div className={classes.content}>{children}</div>

      {Footer ? <Footer /> : null}
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  classes: PropTypes.object.isRequired,
  footer: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
}

export default withStyles(styleSheet)(Layout)
