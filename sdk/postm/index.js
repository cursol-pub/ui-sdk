export const onMessageHandler = handler => {
  window.addEventListener('message', handler, false)
}

export const onMessageHandlerRemove = handler => {
  window.removeEventListener('message', handler, false)
}
