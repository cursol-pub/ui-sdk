import { Component } from 'react'
import PropTypes from 'prop-types'
import { onMessageHandler, onMessageHandlerRemove } from './..'
import { connect } from 'react-redux'

class PostmRouterContainer extends Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    origins: PropTypes.array.isRequired,
    onDispatchAction: PropTypes.func.isRequired,
  }

  componentDidMount() {
    onMessageHandler(this.onMessage)
  }

  componentWillUnmount() {
    onMessageHandlerRemove(this.onMessage)
  }

  onMessage = event => {
    const { routes, origins, onDispatchAction } = this.props
    localStorage && localStorage.csdebug && console.log('iClaus: on message', event)

    if (!event || !event.origin) {
      localStorage && localStorage.csdebug && console.error('iClaus: Wrong Post Message', event)
      return
    }

    if (Array.isArray(origins) && origins.length && origins.indexOf(event.origin) === -1) {
      localStorage && localStorage.csdebug && console.log('iClaus: not allowed origin', origins, event.origin)
      return
    }

    const { data } = event
    if (!data || !data.action || !data.requestId) {
      localStorage && localStorage.csdebug && console.log('iClaus: no action of requestId', event)
      return
    }

    if (!routes[data.action]) {
      localStorage && localStorage.csdebug && console.error('iClaus: No Post Message handler for action', data.action)
      return
    }

    const action = routes[data.action]
    localStorage && localStorage.csdebug && console.log('iClaus: dispatch action', action)
    onDispatchAction(action, data.body, response => {
      localStorage && localStorage.csdebug && console.log('iClaus: on dispatch response', action, response)
      event.source.postMessage(
        {
          action: 'response',
          body: response,
          requestId: data.requestId,
        },
        event.origin
      )
    })
  }

  render() {
    return <div />
  }
}

const mapDispatchToProps = dispatch => ({
  onDispatchAction: (action, data, resp) => dispatch(action(data, resp)),
})

export default connect(
  null,
  mapDispatchToProps
)(PostmRouterContainer)
