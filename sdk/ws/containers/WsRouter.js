import { Component } from 'react'
import PropTypes from 'prop-types'
import { onMessage } from './../ws'
import { connect } from 'react-redux'

class WsRouterContainer extends Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    onDispatchAction: PropTypes.func.isRequired,
  }

  componentDidMount() {
    onMessage(this.onMessage)
  }

  onMessage = message => {
    const { routes, onDispatchAction } = this.props

    if (!message || !message.data) {
      console.error('Wrong WS Protocol response', message)
      return
    }

    const data = JSON.parse(message.data)

    if (!data || !data.action || !data.code || !data.body) {
      console.error('Wrong WS response (action | code | body left)', message.data)
      return
    }

    if (!routes[data.action]) {
      console.error('No WS handler for action', message.action)
      return
    }

    const { success, fail } = routes[data.action]

    if (data.code < 400 && success) {
      onDispatchAction(success, data.body.data)
    }

    if (data.code >= 400 && fail) {
      onDispatchAction(fail, data.body.error)
    }
  }

  render() {
    return <div />
  }
}

const mapDispatchToProps = dispatch => ({
  onDispatchAction: (action, data) => dispatch(action(data)),
})

export default connect(
  null,
  mapDispatchToProps
)(WsRouterContainer)
