import AppConfig from 'AppConfig'
import { hasToken, getToken } from './../auth/jwt'

let ws = null
let ready = false
let onReadyQueue = []

export const getWebSocket = () => {
  if (!AppConfig.ws) {
    return
  }

  if (!ws) {
    ws = new WebSocket(AppConfig.ws)

    if (!ws) {
      console.log('Cannot get WS')
      return
    }

    if (hasToken()) {
      wsLogin(getToken())
    }

    ws.onopen = () => {
      setTimeout(onReady, 1000)
    }
  }

  return ws
}

const onReady = () => {
  ready = true
  const queueToProcess = onReadyQueue.slice(0)
  onReadyQueue = []

  queueToProcess.map(({ action, data }) => {
    sendMessage(action, data)
  })
}

export const sendMessage = (action, data = null) => {
  if (!getWebSocket()) {
    return
  }

  if (ready) {
    getWebSocket().send(
      JSON.stringify({
        action,
        data,
      })
    )
  } else {
    onReadyQueue.push({
      action,
      data,
    })
  }
}

export const onMessage = handler => {
  if (!getWebSocket()) {
    return
  }

  getWebSocket().onmessage = handler
}

export const wsLogin = jwt => {
  sendMessage('login', { jwt })
}

export const wsLogout = () => {
  sendMessage('logout')
}
