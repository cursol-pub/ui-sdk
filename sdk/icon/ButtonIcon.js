import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

const styleSheet = theme => ({
  icon: {
    marginLeft: theme.spacing(1),
  },
})

const ButtonIcon = props => {
  const { component: ComponentClass, classes, componentProps } = props

  return <ComponentClass className={classes.icon} {...componentProps} />
}
ButtonIcon.propTypes = {
  classes: PropTypes.object.isRequired,
  component: PropTypes.any.isRequired,
  componentProps: PropTypes.object,
}

export default withStyles(styleSheet)(ButtonIcon)
