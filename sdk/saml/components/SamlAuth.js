import AppConfig from 'AppConfig'

import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Divider from './../../layout/Divider'

import ContentBlock from './../../layout/ContentBlock'
import SamlResponseHandler from './../../saml/containers/SamlResponseHandler'

const SamlAuth = () => {
  return (
    <ContentBlock md>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Typography variant="h4" gutterBottom>
            Welcome to {AppConfig.appName}
          </Typography>
          <Divider />
        </Grid>

        <Grid item xs={12}>
          <SamlResponseHandler />
        </Grid>
      </Grid>
    </ContentBlock>
  )
}

export default SamlAuth
