import AppConfig from 'AppConfig'
import PropTypes from 'prop-types'

import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Divider from './../../layout/Divider'

import ContentBlock from './../../layout/ContentBlock'
import SamlRequestMaker from './../../saml/containers/SamlRequestMaker'

const SamlLogin = ({ location }) => {
  const from = (location.state && location.state.from) || { pathname: '/' }

  return (
    <ContentBlock md>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Typography variant="h4" gutterBottom>
            Welcome to {AppConfig.appName}
          </Typography>
          <Divider />
        </Grid>

        <Grid item xs={12}>
          <Typography variant="body1" gutterBottom>
            Now you will be redirected to SSO service for authorization
          </Typography>

          <Typography variant="body2" gutterBottom>
            <SamlRequestMaker origin={from.pathname} />
          </Typography>
        </Grid>
      </Grid>
    </ContentBlock>
  )
}

SamlLogin.propTypes = {
  location: PropTypes.object,
}

export default SamlLogin
