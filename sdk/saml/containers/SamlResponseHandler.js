import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import Grid from '@material-ui/core/Grid'
import Message from './../../layout/Message'
import { parseQueryString } from './../../http/query'
import Auth from './../../auth/containers/Auth'
import { signInByToken } from './../../auth/actions/auth'

class SamlResponseHandlerContainer extends Component {
  static propTypes = {
    location: PropTypes.object,
    onSignInByToken: PropTypes.func.isRequired,
    isAuthorised: PropTypes.bool.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    error: PropTypes.object,
  }

  parsedLocation = null

  componentDidMount() {
    if (this.isAwaitingSignIn()) {
      this.signInByToken()
    }
  }

  isAwaitingSignIn() {
    const { isSubmitting, isAuthorised } = this.props

    return !isSubmitting && !isAuthorised && this.getToken()
  }

  signInByToken() {
    const { onSignInByToken } = this.props
    onSignInByToken(this.getToken())
  }

  getLocation() {
    if (!this.parsedLocation) {
      const { location } = this.props
      this.parsedLocation = parseQueryString(location.search)
    }
    return this.parsedLocation
  }

  getToken() {
    return this.getLocation().token
  }

  getRelayState() {
    return this.getLocation().relayState
  }

  getError() {
    return this.getLocation().error
  }

  render() {
    if (this.getError()) {
      return (
        <Grid container spacing={1}>
          <Grid item>
            <Message
              variant="error"
              message={`Auth Error, please report an issue: ${this.getError() || this.getError().toString()}`}
            />
          </Grid>
        </Grid>
      )
    }

    const [pathname, search] = this.getRelayState().split('?')
    const redirectTo = {
      pathname,
      search,
    }

    return (
      <Grid container spacing={1}>
        <Grid item className={'loading-block bus'}>
          <Auth guest>We are signing you in ...</Auth>

          <Auth user>
            Redirecting to <b>{this.getRelayState()}</b> ...
            <Redirect to={redirectTo} />
          </Auth>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  const {
    auth: { isSubmitting, isAuthorised, error },
  } = state
  return {
    isSubmitting,
    isAuthorised,
    error,
  }
}

const mapDispatchToProps = dispatch => ({
  onSignInByToken: token => dispatch(signInByToken(token)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SamlResponseHandlerContainer))
