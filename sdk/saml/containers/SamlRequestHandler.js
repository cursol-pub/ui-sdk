import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router'

import Grid from '@material-ui/core/Grid'
import Message from './../../layout/Message'

import { parseQueryString } from './../../http/query'
import Auth from './../../auth/containers/Auth'
import { getLoginRequest } from './../actions/saml'
import SamlResponseMaker from './SamlResponseMaker'

class SamlRequestHandlerContainer extends Component {
  static propTypes = {
    location: PropTypes.object,
    onGetLoginRequest: PropTypes.func.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    samlRequest: PropTypes.object,
    samlResponse: PropTypes.object,
    error: PropTypes.object,
    from: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  }

  componentDidMount() {
    if (this.isAwaitingRequestId()) {
      this.getLoginRequest()
    }
  }

  isSamlAuth() {
    const { location, samlRequest, samlResponse } = this.props
    const queryParams = parseQueryString(location.search)

    return (
      queryParams.SAMLRequest ||
      (samlRequest && samlRequest.samlRequestId) ||
      (samlResponse && samlResponse.destinationUrl)
    )
  }

  isAwaitingRequestId() {
    const { location, isSubmitting, samlRequest } = this.props
    const queryParams = parseQueryString(location.search)

    return !isSubmitting && queryParams.SAMLRequest && !(samlRequest && samlRequest.samlRequestId)
  }

  getLoginRequest() {
    const { location, onGetLoginRequest } = this.props
    const queryParams = parseQueryString(location.search, false)
    onGetLoginRequest(queryParams.SAMLRequest, queryParams.RelayState)
  }

  render() {
    const { error, isSubmitting, from } = this.props

    if (error) {
      return (
        <Grid container spacing={1}>
          <Grid item>
            <Message
              variant="error"
              message={`SAML Error, please report an issue: ${error.message || error.toString()}`}
            />
          </Grid>
        </Grid>
      )
    }

    if (!this.isSamlAuth()) {
      return null
    }

    if (isSubmitting || this.isAwaitingRequestId()) {
      return (
        <Grid container spacing={1}>
          <Grid item className={'loading-block'}>
            Please wait, we are processing your request...
          </Grid>
        </Grid>
      )
    }

    return (
      <div>
        <Auth guest>
          <Redirect
            to={{
              pathname: '/login',
              state: { from: from || '/saml' },
            }}
          />
        </Auth>
        <Auth user>
          <SamlResponseMaker />
        </Auth>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const {
    saml: { isSubmitting, samlRequest, samlResponse, error },
  } = state
  return {
    isSubmitting,
    samlRequest,
    samlResponse,
    error,
  }
}

const mapDispatchToProps = dispatch => ({
  onGetLoginRequest: (samlRequest, relayState) => dispatch(getLoginRequest(samlRequest, relayState)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SamlRequestHandlerContainer))
