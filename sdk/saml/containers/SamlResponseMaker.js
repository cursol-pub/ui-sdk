import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Grid from '@material-ui/core/Grid'

import Auth from './../../auth/containers/Auth'
import Message from './../../layout/Message'
import { getLoginResponse } from './../actions/saml'

class SamlResponseContainer extends Component {
  static propTypes = {
    location: PropTypes.object,
    onGetLoginResponse: PropTypes.func.isRequired,
    user: PropTypes.object,
    isSubmitting: PropTypes.bool.isRequired,
    samlRequest: PropTypes.object,
    samlResponse: PropTypes.object,
    error: PropTypes.object,
  }

  componentDidMount() {
    if (this.isAwaitingResponseUrl()) {
      this.getLoginResponse()
    }
  }

  isAwaitingResponseUrl() {
    const { isSubmitting, samlRequest, samlResponse, user } = this.props

    return (
      !isSubmitting &&
      user &&
      samlRequest &&
      samlRequest.samlRequestId &&
      !(samlResponse && samlResponse.destinationUrl)
    )
  }

  getLoginResponse() {
    const { samlRequest, onGetLoginResponse, user } = this.props
    onGetLoginResponse(samlRequest.spACS, samlRequest.spRelayState, samlRequest.samlRequestId, user.login)
  }

  render() {
    const { error, isSubmitting, samlResponse } = this.props

    if (error) {
      return (
        <Grid container spacing={1}>
          <Grid item>
            <Message
              variant="error"
              message={`SAML Error, please report an issue: ${error.message || error.toString()}`}
            />
          </Grid>
        </Grid>
      )
    }

    if (isSubmitting || this.isAwaitingResponseUrl()) {
      return (
        <Grid container spacing={1}>
          <Grid item className={'loading-block bus'}>
            Please wait, we are geting you back to the project...
          </Grid>
        </Grid>
      )
    }

    setTimeout(() => {
      document.getElementById('saml-submit').submit()
    }, 200)

    return (
      <Auth user>
        <form method="post" action={samlResponse.destinationUrl} id="saml-submit">
          <input type="hidden" name="SAMLResponse" value={samlResponse.xml} />
          <input type="hidden" name="RelayState" value={samlResponse.relayState} />
        </form>

        <Grid container spacing={1}>
          <Grid item className={'loading-block bus'}>
            Redirecting back to the project {samlResponse.destinationUrl} ...
          </Grid>
        </Grid>
      </Auth>
    )
  }
}

const mapStateToProps = state => {
  const {
    saml: { isSubmitting, samlRequest, samlResponse, error },
    auth: { user },
  } = state
  return {
    user,
    isSubmitting,
    samlRequest,
    samlResponse,
    error,
  }
}

const mapDispatchToProps = dispatch => ({
  onGetLoginResponse: (spACS, spRelayState, samlRequestId, login) =>
    dispatch(getLoginResponse(spACS, spRelayState, samlRequestId, login)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SamlResponseContainer)
