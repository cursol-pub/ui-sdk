import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Grid from '@material-ui/core/Grid'
import Message from './../../layout/Message'
import { postSamlRequest } from './../actions/saml'

class SamlRequestContainer extends Component {
  static propTypes = {
    origin: PropTypes.string,
    onPostSamlRequest: PropTypes.func.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    samlRequest: PropTypes.object,
    error: PropTypes.object,
  }

  componentDidMount() {
    if (this.isAwaitingRedirectUrl()) {
      this.getRedirectUrl()
    }
  }

  isAwaitingRedirectUrl() {
    const { isSubmitting, samlRequest } = this.props

    return !isSubmitting && !samlRequest
  }

  getRedirectUrl() {
    const { origin, onPostSamlRequest } = this.props
    onPostSamlRequest(origin)
  }

  render() {
    const { error, isSubmitting, samlRequest } = this.props

    if (error) {
      return (
        <Grid container spacing={1}>
          <Grid item>
            <Message
              variant="error"
              message={`SAML Error, please report an issue: ${error.message || error.toString()}`}
            />
          </Grid>
        </Grid>
      )
    }

    if (!isSubmitting && samlRequest && samlRequest.redirectTo) {
      location.href = samlRequest.redirectTo
      return (
        <Grid container spacing={1}>
          <Grid item className={'loading-block'}>
            Redirecting to SSO {samlRequest.redirectTo} ...
          </Grid>
        </Grid>
      )
    }

    return (
      <Grid container spacing={1}>
        <Grid item className={'loading-block bus'}>
          Please wait, we are processing your request...
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  const {
    saml: { isSubmitting, samlRequest, error },
  } = state
  return {
    isSubmitting,
    samlRequest,
    error,
  }
}

const mapDispatchToProps = dispatch => ({
  onPostSamlRequest: (origin, referer) => dispatch(postSamlRequest(origin, referer)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SamlRequestContainer)
