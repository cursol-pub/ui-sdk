import AppConfig from 'AppConfig'
import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import { signOut } from './../../auth/actions/auth'
import Auth from './../../auth/containers/Auth'

class SamlLogoutContainer extends Component {
  static propTypes = {
    onSignOut: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { onSignOut } = this.props
    onSignOut()
    location.href = AppConfig.ssoLogout
  }

  render() {
    return (
      <Grid container spacing={1} alignItems="center" justify="center">
        <Grid item xs>
          <Auth user>
            <Typography variant="body2">Signing out...</Typography>
          </Auth>

          <Auth guest>
            <Typography variant="body2">Signed out!</Typography>
          </Auth>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  onSignOut: () => dispatch(signOut()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SamlLogoutContainer)
