import {
  SAML_LOGIN_REQUEST_GET,
  SAML_LOGIN_REQUEST_SUCCESS,
  SAML_LOGIN_REQUEST_FAIL,
  SAML_LOGIN_RESPONSE_GET,
  SAML_LOGIN_RESPONSE_SUCCESS,
  SAML_LOGIN_RESPONSE_FAIL,
  SAML_REQUEST_POST,
  SAML_REQUEST_SUCCESS,
  SAML_REQUEST_FAIL,
  SAML_RESPONSE_POST,
  SAML_RESPONSE_SUCCESS,
  SAML_RESPONSE_FAIL,
} from './../actions/saml'

const initialState = {
  isSubmitting: false,
  samlRequest: null,
  samlResponse: null,
  error: null,
}

const reducers = (state = initialState, actionData = null) => {
  switch (actionData.type) {
    case SAML_LOGIN_REQUEST_GET:
      return {
        ...state,
        isSubmitting: true,
        samlRequest: actionData.data,
        samlResponse: null,
      }

    case SAML_LOGIN_REQUEST_SUCCESS:
      return {
        ...state,
        isSubmitting: false,
        error: null,
        samlRequest: Object.assign({}, state.samlRequest, actionData.data),
      }

    case SAML_LOGIN_REQUEST_FAIL:
      return {
        ...state,
        isSubmitting: false,
        error: actionData.error,
      }

    case SAML_LOGIN_RESPONSE_GET:
      return {
        ...state,
        isSubmitting: true,
        samlResponse: null,
      }

    case SAML_LOGIN_RESPONSE_SUCCESS:
      return {
        ...state,
        isSubmitting: false,
        samlResponse: actionData.data,
        error: null,
      }

    case SAML_LOGIN_RESPONSE_FAIL:
      return {
        ...state,
        isSubmitting: false,
        error: actionData.error,
      }

    case SAML_REQUEST_POST:
      return {
        ...state,
        isSubmitting: true,
        samlRequest: actionData.data,
        samlResponse: null,
      }

    case SAML_REQUEST_SUCCESS:
      return {
        ...state,
        isSubmitting: false,
        error: null,
        samlRequest: Object.assign({}, state.samlRequest, actionData.data),
      }

    case SAML_REQUEST_FAIL:
      return {
        ...state,
        isSubmitting: false,
        error: actionData.error,
      }

    case SAML_RESPONSE_POST:
      return {
        ...state,
        isSubmitting: true,
        samlResponse: actionData.data,
      }

    case SAML_RESPONSE_SUCCESS:
      return {
        ...state,
        isSubmitting: false,
        samlResponse: Object.assign({}, state.samlResponse, actionData.data),
        error: null,
      }

    case SAML_RESPONSE_FAIL:
      return {
        ...state,
        isSubmitting: false,
        error: actionData.error,
      }

    default:
      return state
  }
}

export default reducers
