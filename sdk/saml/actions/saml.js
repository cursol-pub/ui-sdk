import { getRequest, postRequest } from 'cursol/sdk/rest/requests'

// Requests used in SSO to login
export const SAML_LOGIN_REQUEST_GET = 'SAML_LOGIN_REQUEST_GET'
export const SAML_LOGIN_REQUEST_SUCCESS = 'SAML_LOGIN_REQUEST_SUCCESS'
export const SAML_LOGIN_REQUEST_FAIL = 'SAML_LOGIN_REQUEST_FAIL'
export const SAML_LOGIN_RESPONSE_GET = 'SAML_LOGIN_RESPONSE_GET'
export const SAML_LOGIN_RESPONSE_SUCCESS = 'SAML_LOGIN_RESPONSE_SUCCESS'
export const SAML_LOGIN_RESPONSE_FAIL = 'SAML_LOGIN_RESPONSE_FAIL'
// Requests used in service provider to auth by SAML
export const SAML_REQUEST_POST = 'SAML_REQUEST_POST'
export const SAML_REQUEST_SUCCESS = 'SAML_REQUEST_SUCCESS'
export const SAML_REQUEST_FAIL = 'SAML_REQUEST_FAIL'
export const SAML_RESPONSE_POST = 'SAML_RESPONSE_POST'
export const SAML_RESPONSE_SUCCESS = 'SAML_RESPONSE_SUCCESS'
export const SAML_RESPONSE_FAIL = 'SAML_RESPONSE_FAIL'

export const getLoginRequest = (samlRequest, relayState) => dispatch => {
  dispatch({
    type: SAML_LOGIN_REQUEST_GET,
    data: {
      samlRequest,
      relayState,
    },
  })

  return getRequest(
    `saml/login-request?SAMLRequest=${encodeURIComponent(samlRequest)}&RelayState=${encodeURIComponent(relayState)}`
  )
    .then(data => {
      dispatch({
        type: SAML_LOGIN_REQUEST_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: SAML_LOGIN_REQUEST_FAIL,
        error,
      })
    })
}

export const getLoginResponse = (spACS, spRelayState, samlRequestId, login) => dispatch => {
  dispatch({
    type: SAML_LOGIN_RESPONSE_GET,
  })

  return getRequest(
    `saml/login-response` +
      `?spACS=${encodeURIComponent(spACS)}` +
      `&spRelayState=${encodeURIComponent(spRelayState)}` +
      `&samlRequestId=${encodeURIComponent(samlRequestId)}` +
      `&login=${encodeURIComponent(login)}`
  )
    .then(data => {
      dispatch({
        type: SAML_LOGIN_RESPONSE_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: SAML_LOGIN_RESPONSE_FAIL,
        error,
      })
    })
}

export const postSamlRequest = origin => dispatch => {
  dispatch({
    type: SAML_REQUEST_POST,
    data: {
      origin,
    },
  })

  return postRequest('saml/request', { origin })
    .then(data => {
      if (!data.redirectTo || data.redirectTo === '') {
        dispatch({
          type: SAML_REQUEST_FAIL,
          error: 'SAML request has no redirect url',
        })
      }

      dispatch({
        type: SAML_REQUEST_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: SAML_REQUEST_FAIL,
        error,
      })
    })
}

export const postSamlResponse = (RelayState, SAMLResponse) => dispatch => {
  dispatch({
    type: SAML_RESPONSE_POST,
    data: {
      RelayState,
      SAMLResponse,
    },
  })

  return postRequest('saml/response', { RelayState, SAMLResponse })
    .then(data => {
      dispatch({
        type: SAML_RESPONSE_SUCCESS,
        data,
      })
    })
    .catch(error => {
      dispatch({
        type: SAML_RESPONSE_FAIL,
        error,
      })
    })
}
