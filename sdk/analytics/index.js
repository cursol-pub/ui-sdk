import { addAsyncScript } from './../script'

let gtagInitialized = false

export function gtag() {
  if (gtagInitialized && window.dataLayer) {
    window.dataLayer.push(arguments)
  }
}

export const addGtag = gtagId => {
  addAsyncScript(`https://www.googletagmanager.com/gtag/js?id=${gtagId}`, 'gtm')

  window.dataLayer = window.dataLayer || []
  gtagInitialized = true
  gtag('js', new Date())
  gtag('config', gtagId)
}
