import { Component } from 'react'
import PropTypes from 'prop-types'
import AppConfig from 'AppConfig'
import { withRouter } from 'react-router'

import { addAsyncScript, removeScript } from './../../script'

class CrazyEgg extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
  }

  componentDidMount() {
    this.addScript()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.addScript()
    }
  }

  addScript = () => {
    if (AppConfig.crazyEggScript) {
      removeScript('crazy-egg')
      addAsyncScript(AppConfig.crazyEggScript, 'crazy-egg')
    }
  }

  render() {
    return <div />
  }
}

export default withRouter(CrazyEgg)
