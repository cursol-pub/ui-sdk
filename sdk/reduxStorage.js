import AppConfig from 'AppConfig'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { reducer as formReducer } from 'redux-form'

import auth from './auth/reducers/auth'
import saml from './saml/reducers/saml'

const storage = (additionalReducers, additionalMiddlewares) => () => {
  const middlewares = [thunkMiddleware]

  if (AppConfig.env !== 'production') {
    middlewares.push(createLogger())
  }

  if (additionalMiddlewares && Array.isArray(additionalMiddlewares)) {
    additionalMiddlewares.map(m => middlewares.push(m))
  }

  const reducers = {
    form: formReducer,
    auth,
    saml,
    ...additionalReducers,
  }

  return createStore(combineReducers(reducers), applyMiddleware(...middlewares))
}

export default storage
