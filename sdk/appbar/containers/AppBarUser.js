import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import Grid from '@material-ui/core/Grid'
import { green } from '@material-ui/core/colors'
import Fade from '@material-ui/core/Fade'

import AppBarUserMenu from './../components/AppBarUserMenu'

const styleSheet = theme => ({
  avatar: {
    color: theme.palette.getContrastText(theme.palette.primary[500]),
    backgroundColor: green[400],
  },
  userName: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
})

class AppBarUserContainer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    isSubmitting: PropTypes.bool.isRequired,
    user: PropTypes.object,
  }

  static defaultProps = {
    isSubmitting: false,
  }

  render() {
    const { classes, isSubmitting, user } = this.props

    let userWidget = null

    if (user && !isSubmitting) {
      let avatar = user.login[0] + user.login[1]
      let userName = user.login
      if (user.firstName && user.firstName.length > 1) {
        avatar = user.firstName[0] + user.firstName[1]
        userName = user.firstName
        if (user.lastName && user.lastName.length > 0) {
          avatar = user.firstName[0] + user.lastName[0]
          userName = `${user.firstName} ${user.lastName}`
        }
      }

      userWidget = (
        <Fade in timeout={{ enter: 350, exit: 200 }}>
          <Grid container spacing={1} alignItems="center">
            <Grid item>
              <Avatar className={classes.avatar}>{avatar}</Avatar>
            </Grid>

            <Grid item className={classes.userName}>
              <Typography variant="body1" color="inherit">
                {userName}
              </Typography>
            </Grid>
          </Grid>
        </Fade>
      )
    }

    return (
      <Grid container spacing={1} alignItems="center">
        <Grid item>{userWidget}</Grid>

        <Grid item>
          <AppBarUserMenu />
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => {
  const {
    auth: { isSubmitting, user },
  } = state
  return {
    isSubmitting,
    user,
  }
}

export default withStyles(styleSheet)(connect(mapStateToProps)(AppBarUserContainer))
