import AppConfig from 'AppConfig'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { withStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

import AppBarUser from './../containers/AppBarUser'

const styleSheet = theme => ({
  title: {
    textTransform: 'none',
    color: theme.palette.getContrastText(theme.palette.primary[500]),
    textDecoration: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    paddingLeft: 8,
  },
  menu: {
    marginLeft: 'auto',
  },
})

const AppBarContent = ({ classes, appMenu: AppMenuComponent }) => {
  return (
    <Grid container spacing={1} justify="space-between" alignItems="center">
      <Grid item>
        <Toolbar>
          {AppMenuComponent ? <AppMenuComponent /> : null}

          <Link to="/" className={classes.title}>
            <Typography variant="h5" color="inherit">
              {AppConfig.appName}
            </Typography>
          </Link>
        </Toolbar>
      </Grid>

      <Grid item>
        <Toolbar className={classes.menu}>
          <AppBarUser />
        </Toolbar>
      </Grid>
    </Grid>
  )
}

AppBarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  appMenu: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
}

export default withStyles(styleSheet)(AppBarContent)
