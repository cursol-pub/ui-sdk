import { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'

import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/MoreVert'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import Auth from 'cursol/sdk/auth/containers/Auth'

const styleSheet = theme => ({
  menu: {
    marginTop: 8,
    marginLeft: -52,
  },
  item: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
    fontWeight: 400,
  },
})

class AppBarUserMenuContainer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    location: PropTypes.object,
  }

  state = {
    anchorEl: undefined,
    open: false,
  }

  handleMenuClick = event => {
    this.setState({ open: true, anchorEl: event.currentTarget })
  }

  handleChooseItem = () => {
    this.setState({ open: false })
  }

  render() {
    const { classes, location } = this.props

    const reportIssueItem = (
      <MenuItem
        onClick={this.handleChooseItem}
        component="a"
        href="http://jira.currencysolutions.com/servicedesk/customer/portal/1"
        target="blank"
        className={classes.item}
      >
        Report issue
      </MenuItem>
    )

    return (
      <div>
        <IconButton
          color="inherit"
          title="User menu"
          aria-owns="user-menu"
          aria-haspopup="true"
          onClick={this.handleMenuClick}
        >
          <MenuIcon />
        </IconButton>

        <Auth guest>
          <Menu
            id="user-menu"
            anchorEl={this.state.anchorEl}
            open={this.state.open}
            onClose={this.handleChooseItem}
            elevation={2}
            className={classes.menu}
          >
            <MenuItem
              onClick={this.handleChooseItem}
              component={Link}
              to={{
                pathname: '/login',
                state: { from: location },
              }}
              className={classes.item}
            >
              Sign In
            </MenuItem>

            {reportIssueItem}
          </Menu>
        </Auth>

        <Auth user>
          <Menu
            id="user-menu"
            anchorEl={this.state.anchorEl}
            open={this.state.open}
            onClose={this.handleChooseItem}
            elevation={2}
            className={classes.menu}
          >
            <MenuItem onClick={this.handleChooseItem} component={Link} to="/logout" className={classes.item}>
              Sign Out
            </MenuItem>

            {reportIssueItem}
          </Menu>
        </Auth>
      </div>
    )
  }
}

export default withStyles(styleSheet)(withRouter(AppBarUserMenuContainer))
