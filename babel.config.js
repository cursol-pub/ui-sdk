module.exports = {
  presets: [
    [
      '@babel/env',
      {
        loose: true,
        useBuiltIns: 'entry',
        corejs: 3,
        targets: '> 0.25%, not dead'
      }
    ],
    '@babel/react'
  ],
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['@babel/plugin-proposal-object-rest-spread', { loose: true }],
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-transform-async-to-generator',
    // for IE 11 support
    '@babel/plugin-transform-object-assign',
  ]
};
